package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName


class GPRS {

    @SerializedName("TotalAmt")
    var kilobytes: Int? = null

    @SerializedName("Detail")
    var details: List<Detail>? = null

    @SerializedName("Percent")
    var percents: List<String>? = null
}