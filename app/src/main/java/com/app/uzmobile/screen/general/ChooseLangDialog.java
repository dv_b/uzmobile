package com.app.uzmobile.screen.general;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.uzmobile.R;

public class ChooseLangDialog extends DialogFragment {

  private static final String REGIONS = "regions";

  TextView title;

  RecyclerView list;

  LinearLayout ru;

  LinearLayout uz;

  private Delegate mDelegate;

  public static ChooseLangDialog getInstance() {
    return new ChooseLangDialog();
  }


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setStyle(STYLE_NO_TITLE, getTheme());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.dialog_choice, null);
    LinearLayout ru = v.findViewById(R.id.ru);
    ru.setOnClickListener(v1 -> {
      mDelegate.chooseLang("ru", "Ru");
      dismiss();
    });
    LinearLayout uz = v.findViewById(R.id.uz);
    uz.setOnClickListener(v1 -> {
      mDelegate.chooseLang("uz", "Uz");
      dismiss();
    });
    return v;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }




  public ChooseLangDialog setDelegate(Delegate delegate) {
    mDelegate = delegate;
    return this;
  }

  public interface Delegate {
    void chooseLang(String lang, String country);
  }


}
