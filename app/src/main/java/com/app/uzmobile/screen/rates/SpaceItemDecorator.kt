package com.app.uzmobile.screen.rates

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View


class SpaceItemDecorator(space: Int) : RecyclerView.ItemDecoration() {

    val space = space

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        outRect?.bottom = space
        if (parent?.getChildLayoutPosition(view)?.rem(2) == 0) {
            outRect?.right = space
            outRect?.left = 0
        }

        outRect?.top = 0


    }
}