package com.app.uzmobile.screen.chart

import android.os.Bundle
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.R
import com.app.uzmobile.widgets.CircleProgressbar
import java.util.*

class ChatAlterActivity : BaseActivity() {

    var container: RelativeLayout? = null

    var chart: CircleProgressbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart_alter)

        container = findViewById(R.id.container)

        chart = findViewById(R.id.chart)

        val list = Arrays.asList("50.0", "25.0", "12.0")
        chart?.setAllProgressWithAnimation(list)

        for (item in list) {
            val label = TextView(this)
            label.text = item
            container?.addView(label)
        }




    }
}

