package com.app.uzmobile.screen.rates.adapters

import com.app.uzmobile.content.Service


interface AdapterCalback {
    fun call(item: Service, position: Int)
}