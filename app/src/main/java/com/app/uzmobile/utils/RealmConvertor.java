package com.app.uzmobile.utils;


import com.app.uzmobile.content.Service;
import com.app.uzmobile.persistant.Account;
import com.app.uzmobile.persistant.RealmPack;
import com.app.uzmobile.persistant.RealmService;
import com.app.uzmobile.persistant.RealmTarif;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmResults;

public class RealmConvertor {

  public static List<Service> getServices(RealmResults<RealmService> realmServices) {
    List<Service> services = new ArrayList<>();
    for (RealmService realmService : realmServices) {
      services.add(getService(realmService));
    }

    return services;
  }

  private static Service getService(RealmService realmService) {
    Service service = new Service();
    service.setChecked(realmService.isChecked());
    service.setExpireDate(realmService.getExpireDate());
    service.setExpireDateTimeshtamp(realmService.getExpireDateTimeshtamp());
    service.setMonthlyCost(realmService.getMonthlyCost());
    service.setName(realmService.getName());
    service.setOfferingId(realmService.getOfferingId());
    service.setOneTimeCost(realmService.getOneTimeCost());
    service.setPaymentOnAdd(realmService.getPaymentOnAdd());

    return service;
  }

  public static RealmList<RealmService> getRealmServices(List<Service> services) {
    RealmList<RealmService> realmServices = new RealmList<>();
    for (Service service: services) {
      realmServices.add(getRealmService(service));
    }

    return realmServices;
  }

  private static RealmService getRealmService(Service service) {
    RealmService realmService = new RealmService();
    realmService.setChecked(service.isChecked());
    realmService.setExpireDate(service.getExpireDate());
    realmService.setExpireDateTimeshtamp(service.getExpireDateTimeshtamp());
    realmService.setMonthlyCost(service.getMonthlyCost());
    realmService.setName(service.getName());
    realmService.setOfferingId(service.getOfferingId());
    realmService.setOneTimeCost(service.getOneTimeCost());
    realmService.setPaymentOnAdd(service.getPaymentOnAdd());

    return realmService;
  }



  public static List<Service> getPacks(RealmResults<RealmPack> realmServices) {
    List<Service> services = new ArrayList<>();
    for (RealmPack realmService : realmServices) {
      services.add(getPack(realmService));
    }

    return services;
  }

  private static Service getPack(RealmPack realmService) {
    Service service = new Service();
    service.setChecked(realmService.isChecked);
    service.setExpireDate(realmService.expireDate);
    service.setExpireDateTimeshtamp(realmService.expireDateTimeshtamp);
    service.setMonthlyCost(realmService.monthlyCost);
    service.setName(realmService.name);
    service.setOfferingId(realmService.offeringId);
    service.setOneTimeCost(realmService.oneTimeCost);
    service.setPaymentOnAdd(realmService.paymentOnAdd);

    return service;
  }

  public static RealmList<RealmPack> getRealmPacks(List<Service> services) {
    RealmList<RealmPack> realmServices = new RealmList<>();
    for (Service service: services) {
      realmServices.add(getRealmPack(service));
    }

    return realmServices;
  }

  private static RealmPack getRealmPack(Service service) {
    RealmPack realmService = new RealmPack();
    realmService.isChecked = (service.isChecked());
    realmService.expireDate = (service.getExpireDate());
    realmService.expireDateTimeshtamp = (service.getExpireDateTimeshtamp());
    realmService.monthlyCost = (service.getMonthlyCost());
    realmService.name = (service.getName());
    realmService.offeringId = (service.getOfferingId());
    realmService.oneTimeCost = (service.getOneTimeCost());
    realmService.paymentOnAdd = (service.getPaymentOnAdd());

    return realmService;
  }



  public static List<Service> getTarifs(RealmResults<RealmTarif> realmServices) {
    List<Service> services = new ArrayList<>();
    for (RealmTarif realmService : realmServices) {
      services.add(getTarif(realmService));
    }

    return services;
  }

  private static Service getTarif(RealmTarif realmService) {
    Service service = new Service();
    service.setChecked(realmService.isChecked);
    service.setExpireDate(realmService.expireDate);
    service.setExpireDateTimeshtamp(realmService.expireDateTimeshtamp);
    service.setMonthlyCost(realmService.monthlyCost);
    service.setName(realmService.name);
    service.setOfferingId(realmService.offeringId);
    service.setOneTimeCost(realmService.oneTimeCost);
    service.setPaymentOnAdd(realmService.paymentOnAdd);

    return service;
  }

  public static RealmList<RealmTarif> getRealmTarifs(List<Service> services) {
    RealmList<RealmTarif> realmServices = new RealmList<>();
    for (Service service: services) {
      realmServices.add(getRealmTarif(service));
    }

    return realmServices;
  }

  private static RealmTarif getRealmTarif(Service service) {
    RealmTarif realmService = new RealmTarif();
    realmService.isChecked = service.isChecked();
    realmService.expireDate = (service.getExpireDate());
    realmService.expireDateTimeshtamp = (service.getExpireDateTimeshtamp());
    realmService.monthlyCost = (service.getMonthlyCost());
    realmService.name = (service.getName());
    realmService.offeringId = (service.getOfferingId());
    realmService.oneTimeCost = (service.getOneTimeCost());
    realmService.paymentOnAdd = service.getPaymentOnAdd();

    return realmService;
  }

  public static RealmList<Account> getAccount(Account account) {
    RealmList<Account> accounts = new RealmList<>();
    accounts.add(account);
    return accounts;
  }

}
