package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName

class Detail {

    @SerializedName("OfferingName")
    var offeringName: String? = null

    var persent: Float? = null

    @SerializedName("Unused")
    var unused: Float? = null

    @SerializedName("Initial")
    var initial: Int? = null

    @SerializedName("ExpireDateTimeshtamp")
    var expire: Long? = null
}
