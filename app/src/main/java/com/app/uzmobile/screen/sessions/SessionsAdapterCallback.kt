package com.app.uzmobile.screen.sessions

import com.app.uzmobile.content.Session


interface SessionsAdapterCallback {
    fun call(session: Session?)
    fun deleteAll(subList: MutableList<Session>)
}