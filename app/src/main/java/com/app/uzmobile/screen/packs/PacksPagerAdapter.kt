package com.app.uzmobile.screen.packs

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class PacksPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    var fragments = ArrayList<Fragment>()
    var titleList = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        return fragments.get(position)
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return titleList.get(position)
    }

    fun addItem(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titleList.add(title)
    }

}

