package com.app.uzmobile.screen.chart

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.MainActivity
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.widgets.CustomValueFormatter
import com.app.uzmobile.widgets.SimpleDividerItemDecoration
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class ChartActivity : BaseActivity() {

    companion object {
        const val TITLE = "title"
        const val TOTAL = "total"
        const val PERCENTS = "percents"
        const val FLAG = "flag"
    }

    var chart: PieChart? = null

    var title: TextView? = null

    var backButton: ImageView? = null

    var list: RecyclerView? = null

    var listAdapter: ChartAdapter? = null

    var titleVal: String? = null

    var total: String? = null

    private var  flag = 1

    var percents = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart)

        backButton = findViewById(R.id.menu)
        backButton?.setOnClickListener { _ -> finish() }

        title = findViewById(R.id.title)
        titleVal = intent.getStringExtra(TITLE)
        flag = intent.getIntExtra(FLAG, 1)
        title?.text = titleVal

        list = findViewById(R.id.list)
        listAdapter = ChartAdapter(this)
        list?.layoutManager  = LinearLayoutManager(this)
        list?.adapter = listAdapter
        list?.addItemDecoration(SimpleDividerItemDecoration(this))


//        total = intent.getStringExtra(TOTAL)
//        percents = intent.getStringArrayListExtra(PERCENTS)

        chart = findViewById(R.id.chart)

//        freeUnit()
        percentFreeUnitRequest()
    }

    private fun freeUnit() {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.FreeUnit
                .name))
        val jsend = JSend(request)
        ApiFactory.inPayService!!.freeUnitRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map { res -> res.response?.query?.items!! }
                .subscribe(this::handleUnits, { _ -> Log.d("TAG", "error")})
    }

    private fun handleUnits(response: List<Packet>) {
//        listAdapter?.list = response.filter { it -> flagFilter(it) } as ArrayList<Packet>
//        listAdapter?.notifyDataSetChanged()

        initChart(response.filter { it -> flagFilter(it) })
    }

    private fun flagFilter(pack: Packet): Boolean {
        if (flag == 1)
            return pack.type?.equals("SMS")!!
        else if (flag == 2)
            return pack.type?.equals("GPRS")!!
        else if (flag == 3)
            return pack.type?.equals("Voice")!!
        else
            return pack.type?.equals("SMS")!!
    }

    private fun initChart(list: List<Packet>) {
        chart?.setUsePercentValues(true)
        chart?.description!!.isEnabled = false
        chart?.setExtraOffsets(5.0f, 10.0f, 5.0f, 5.0f)

        chart?.dragDecelerationFrictionCoef = 0.95f

//        chart?.setCenterTextTypeface(mTfLight)
        chart?.setCenterTextSize(25.0f)
        var centerText = getMaxValue(list).toString()
        if (flag == 1)
            centerText = centerText + " СМС"
        else if (flag == 2)
            centerText = centerText + " MБ"
        else if (flag == 3)
            centerText = centerText + " Мин"
        chart?.centerText = centerText
        chart?.setCenterTextColor(Color.BLACK)

//        chart?.isDrawHoleEnabled = true
//        chart?.setHoleColor(Color.WHITE)

//        chart?.setTransparentCircleColor(Color.WHITE)
//        chart?.setTransparentCircleAlpha(255)

        chart?.holeRadius = 58f
        chart?.transparentCircleRadius = 58f

        chart?.setDrawCenterText(true)

        chart?.rotationAngle = 270.0f
        // enable rotation of the chart? by touch
        chart?.isRotationEnabled = true
        chart?.isHighlightPerTapEnabled = true

        // chart?.setUnit(" €");
//         chart?.setDrawUnitsInChart(true);

        // add a selection listener
//        chart?.setOnChartValueSelectedListener(this);

        setData(list)

        val l = chart?.legend
//        l?.verticalAlignment = Legend.LegendVerticalAlignment.TOP
//        l?.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
//        l?.orientation = Legend.LegendOrientation.VERTICAL
        l?.setDrawInside(true)
        l?.xEntrySpace = 7f
        l?.yEntrySpace = 0f
        l?.yOffset = 1000f


        chart?.visibility = View.VISIBLE
        chart?.animateY(1400, Easing.EasingOption.EaseInOutQuad)

    }

    private fun setData(list: List<Packet>) {

        val entries = ArrayList<PieEntry>()

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (pack in list) {
            entries.add(PieEntry((getSum(pack.unused)),
                    "",
                    resources.getDrawable(R.drawable.ic_arrow_left)))
        }

        val dataSet = PieDataSet(entries, titleVal)

        dataSet.setDrawIcons(false)
//        dataSet.isHighlightEnabled = false

        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f


        // add a lot of colors

        val colors = ArrayList<Int>()

//        for (c in ColorTemplate.VORDIPLOM_COLORS)
//            colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS)
            colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS)
            colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())

        dataSet.colors = getColors()
        //dataSet.setSelectionShift(0f);

        val data = PieData(dataSet)
        data.setValueFormatter(CustomValueFormatter(getMaxValue(list), getString(R.string.sms_title)))
        data.setValueTextSize(9f)
        data.setValueTextColor(Color.WHITE)

//        data.setValueTypeface(mTfLight)
        chart?.data = data

        // undo all highlights
        chart?.highlightValues(null)

        chart?.invalidate()
    }

    private fun  getMaxValue(list: List<Packet>): Int {
        var sum: Float = 0.0f
        for (pack in list) {
            sum = sum.plus(getSum(pack.unused))
        }
//        list.asSequence().forEach { sum = sum.plus(getSum(it.unused))}
        if (flag == 2)
            return (sum / 1024).toInt()
        else if (flag == 3)
            return (sum / 60).toInt()
        return sum.toInt()
    }

    private fun getSum(vals: List<Unused>?): Float {
        var sum: Float = 0.0f
        for (unused in vals!!) {
            sum = sum.plus(unused.value?.toFloat())
        }
//        vals!!
//                .asSequence()
//                .map { it.value?.toFloat() }
//                .forEach { sum = sum.plus(it) }
        if (flag == 2)
            return (sum / 1024)
        else if (flag == 3)
            return (sum / 60)
        return sum
    }

    private fun percentFreeUnitRequest() {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.PercentFreeUnit.name, null))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.percentFreeUnits(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handlePercentFreeUnit, { _ -> Log.d("TAG", "error")})
    }

    private fun handlePercentFreeUnit(res: JReceive<PercentFreeUnit>) {

        val err = res.response?.error
        if (err?.equals("6.1")!!) {
            getPrefs().edit().clear().apply()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
            return
        }

        var packUsageData = res.response?.query?.items?.sms

        if (flag == 1) {
            packUsageData = res.response?.query?.items?.sms
        } else if (flag == 2) {
            packUsageData = res.response?.query?.items?.gprs
        } else if (flag == 3) {
            packUsageData = res.response?.query?.items?.voice
        }

        if (packUsageData?.details != null) {
            listAdapter?.list = getCalculatedList(packUsageData?.details as java.util.ArrayList<Detail>?)!!
            listAdapter?.notifyDataSetChanged()
        }

        initChartAlter(packUsageData)

//        val voice = res.response?.query?.items?.voice?.details
//        if (voice != null && voice.isNotEmpty() && vMinutes != null) {
////            minutesPercent?.setAllProgressWithAnimation(getPercents(vMinutes!!, voice))
////            minutes?.text = vMinutes.toString()
//            getPrefs().edit().putInt(C.MINUTES, vMinutes).apply()
//        }


    }

    private fun getCalculatedList(details: java.util.ArrayList<Detail>?): java.util.ArrayList<Detail>? {
        for (i in details?.indices!!) {
            details?.get(i).unused = getCalculatedValue(details?.get(i).unused!!).toFloat()
        }
        return details
    }

    private fun getPercents(total: Int, details: List<Detail>): MutableList<String>? {
        val list = java.util.ArrayList<String>()
        for (detail in details) {
//            list.add(getPercent(total, detail.unused!!).toString())
            list.add(detail.persent!!.toString())
        }
        return list
    }

    private fun initChartAlter(usage: PackUsageData?) {
        chart?.setUsePercentValues(true)
        chart?.description!!.isEnabled = false
        chart?.setExtraOffsets(5.0f, 10.0f, 5.0f, 5.0f)

        chart?.dragDecelerationFrictionCoef = 0.95f

//        chart?.setCenterTextTypeface(mTfLight)
        chart?.setCenterTextSize(25.0f)

        chart?.centerText = getCenterText(usage?.totalAmount)
        chart?.setCenterTextColor(Color.BLACK)

        chart?.holeRadius = 58f
        chart?.transparentCircleRadius = 58f

        chart?.setDrawCenterText(true)

        chart?.rotationAngle = 270.0f
        // enable rotation of the chart? by touch
        chart?.isRotationEnabled = true
        chart?.isHighlightPerTapEnabled = true


        if (usage != null)
            setDataAlter(usage!!)

        val l = chart?.legend

        l?.setDrawInside(true)
        l?.xEntrySpace = 7f
        l?.yEntrySpace = 0f
        l?.yOffset = 1000f


        chart?.visibility = View.VISIBLE
        chart?.animateY(1400, Easing.EasingOption.EaseInOutQuad)

    }

    private fun setDataAlter(usageData: PackUsageData) {

        val entries = ArrayList<PieEntry>()

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        if (usageData.details != null)
        for (pack in usageData.details!!) {
            entries.add(PieEntry(pack.unused!!.toFloat(),
                    "",
                    resources.getDrawable(R.drawable.ic_arrow_left)))
        }

        val dataSet = PieDataSet(entries, titleVal)

        dataSet.setDrawIcons(false)
//        dataSet.isHighlightEnabled = false

        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f


        // add a lot of colors

        val colors = ArrayList<Int>()

//        for (c in ColorTemplate.VORDIPLOM_COLORS)
//            colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS)
            colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS)
            colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())

        dataSet.colors = getColors()
        //dataSet.setSelectionShift(0f);

        val data = PieData(dataSet)
        data.setValueFormatter(CustomValueFormatter(getCalculatedValue(usageData.totalAmount?.toFloat()!!).toInt(), getProperValue()))
        data.setValueTextSize(9f)
        data.setValueTextColor(Color.WHITE)

//        data.setValueTypeface(mTfLight)
        chart?.data = data

        // undo all highlights
        chart?.highlightValues(null)

        chart?.invalidate()
    }



    private fun getCalculatedValue(amount: Float): Double {
//        var sum: Float = 0.0f
//        for (unused in vals!!) {
//            sum = sum.plus(unused.value?.toFloat())
//        }
//        vals!!
//                .asSequence()
//                .map { it.value?.toFloat() }
//                .forEach { sum = sum.plus(it) }
        if (flag == 2)
            return (amount.toDouble() / 1024)
        else if (flag == 3)
            return (amount.toDouble() / 60)
        return amount.toDouble()
    }

    private fun getProperValue(): String {
        if (flag == 1) {
            return getString(R.string.sms_title)
        } else if (flag == 2) {
            return getString(R.string.internet_value)
        } else if (flag == 3) {
            return getString(R.string.voice_val)
        } else {
            return getString(R.string.sms_title)
        }
    }


    private fun getCenterText(totalAmount: Int?): String {
        if (flag == 1)
            return totalAmount?.toString() + "\nСМС"
        else if (flag == 2)
            return totalAmount?.div(1024).toString() + "\nMБ"
        else if (flag == 3)
            return totalAmount?.div(60).toString() + "\nМин"
        else
            return ""
    }


    private fun getColors(): List<Int> {
        if (flag == 1)
            return getSMSColors()
        else if (flag == 2)
            return getMBColors()
        else if (flag == 3)
            return getVoiceColors()
        else
            return getSMSColors()
    }

    private fun getSMSColors(): List<Int> {
        val colors = ArrayList<Int>()
        colors.add(ContextCompat.getColor(this, R.color.sms_stroke))
        colors.add(ContextCompat.getColor(this, R.color.sms_second_pack))
        colors.add(Color.BLUE)
        colors.add(Color.RED)
        colors.add(Color.MAGENTA)

        return colors
    }

    private fun getMBColors(): List<Int> {
        val colors = ArrayList<Int>()
        colors.add(ContextCompat.getColor(this, R.color.mb_stroke))
        colors.add(ContextCompat.getColor(this, R.color.mb_second_pack))
        colors.add(ContextCompat.getColor(this, R.color.mb_third_pack))
        colors.add(Color.RED)
        colors.add(Color.MAGENTA)

        return colors
    }

    private fun getVoiceColors(): List<Int> {
        val colors = ArrayList<Int>()
        colors.add(ContextCompat.getColor(this, R.color.minutes_stroke))
        colors.add(Color.CYAN)
        colors.add(Color.MAGENTA)
        colors.add(Color.RED)
        colors.add(Color.BLUE)

        return colors
    }


}


