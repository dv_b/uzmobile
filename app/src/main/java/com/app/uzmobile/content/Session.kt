package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName


class Session {

    @SerializedName("Description")
    var description: String? = null

    @SerializedName("Token")
    var token: String? = null

    var type: Int? = null
}