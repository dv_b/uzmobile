package com.app.uzmobile.screen.landing

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.app.uzmobile.R

import com.app.uzmobile.persistant.Account


class NumbersAdapter(context: Context, items: List<Account>): RecyclerView.Adapter<NumbersAdapter.ViewHolder>() {

    var context = context

    var items = items

    var callback: Callback? = null


    fun refreshItems(items: List<Account>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_number, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val item = items[position]
        holder?.number?.text = context.getString(R.string.number, item.number)
        holder?.itemView?.setOnClickListener({v ->  callback?.onClick(item)})
    }

    interface Callback {
        fun onClick(item: Account)
    }

    inner class ViewHolder(view: View?): RecyclerView.ViewHolder(view) {
        var userfulImage = view?.findViewById<ImageView>(R.id.useful_image)
        var name = view?.findViewById<TextView>(R.id.name)
        var number = view?.findViewById<TextView>(R.id.number)

    }

}