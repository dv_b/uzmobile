package com.app.uzmobile.persistant

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class RealmService : RealmObject() {

    @PrimaryKey
    var offeringId: String? = null

    var name: String? = null

    var monthlyCost: String? = null

    var oneTimeCost: String? = null

    var expireDateTimeshtamp: Long? = null

    var expireDate: Long? = null

    var paymentOnAdd: Int? = null

    var isChecked = false
}