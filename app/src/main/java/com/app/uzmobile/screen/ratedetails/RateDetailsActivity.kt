package com.app.uzmobile.screen.ratedetails

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.screen.packdetails.SectionsAdapter
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class RateDetailsActivity : BaseActivity() {


    companion object {
        const val RATE = "rate"
    }

    var backBtn: ImageView? = null

    var title: TextView? = null

    var rate: TextView? = null

    var rateValue: TextView? = null

    var list: RecyclerView? = null

    var addService: Button? = null

    var error: TextView? = null

    var alert: RelativeLayout? = null

    var successIcon: ImageView? = null

    var handler: Handler = Handler(Looper.getMainLooper())


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packdetail)

        val thisRate = intent.getSerializableExtra(RATE) as Service?

        alert = findViewById(R.id.alert)

        error = findViewById(R.id.alertTitle)
        error?.text = getString(R.string.err_buy_rate)

        successIcon = findViewById(R.id.success_ic)

        backBtn = findViewById(R.id.menu)
        backBtn?.setOnClickListener { _ -> finish() }

        title = findViewById(R.id.title)
        title?.text = getString(R.string.prices)

        addService = findViewById(R.id.add)
        addService?.text = getString(R.string.add_tarif)
        addService?.setOnClickListener { _ -> createAndShowAlertDialog(thisRate?.offeringId) }

        rate = findViewById(R.id.pack)
        rateValue = findViewById(R.id.balance)

        rate?.text = thisRate?.name

        rateValue?.text = getString(R.string.balance_val, thisRate?.monthlyCost)

        list = findViewById(R.id.list)
        list?.layoutManager = LinearLayoutManager(this)

        request(thisRate?.offeringId)


    }

    private fun request(offeringId: String?) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account = null
//        header.Account.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.Service.name, null, "Info", offeringId, null))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.getService(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleTarifs, { t: Throwable? ->   t?.printStackTrace()})
    }

    private fun handleTarifs(res: JReceive<Tarifs>) {
        var adapter = SectionsAdapter(this,res.response?.query?.items?.sections)
        list?.adapter = adapter

    }

    private fun addService(action: String = "Add", offeringId: String? = null) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account = null
//        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.PO.name, null, action, offeringId))
        val jsend = JSend(request)
        ApiFactory.inPayService!!.soRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
//                .map { res -> res.response?.query?.items!! }
                .subscribe(this::handleAddService, { _ -> Log.d("TAG", "error")})
    }


    private fun handleAddService(response : JReceive<List<Service>>) {
        if (!response.response?.error?.equals("0")!! || response.response?.query?.result?.equals("false")!!)
//            Toast.makeText(this, "Не удалось, попробуйте позже", Toast.LENGTH_LONG).show()
            showAlert()
        else {
            showSuccessAlert()
//            Toast.makeText(this, "Вы перешли на выбранный тариф", Toast.LENGTH_LONG).show()
        }
    }

    private fun createAndShowAlertDialog(offeringId: String?) {
        val dialogBuilder = AlertDialog.Builder(this)//ContextThemeWrapper(this, R.style.AlertDialogCustom))
        dialogBuilder.setTitle(getString(R.string.warning_change_rate))
        dialogBuilder.setPositiveButton(R.string.yes, { dialog, id ->
            //TODO
            addService("Change", offeringId)
//            subscribe(offeringId, position)
            dialog.dismiss()
        })
        dialogBuilder.setNegativeButton(R.string.no, { dialog, id ->
            //TODO
            dialog.dismiss()
        })
        val dialog = dialogBuilder.create()
        dialog.show()
    }

    private fun showSuccessAlert() {
        successIcon?.visibility = View.VISIBLE
        error?.text = getString(R.string.rate_change_success)
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, R.color.sucess))


        handler.postDelayed(this::hideAlert, 3000)
    }

    private fun showFailureAlert(text: String) {
        successIcon?.visibility = View.GONE
        error?.text = text
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_dark))



        handler.postDelayed(this::hideAlert, 3000)

    }

    private fun showAlert() {
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.visibility = View.VISIBLE
        alert?.startAnimation(bottomUp)

        handler.postDelayed(this::hideAlert, 3000)


    }

    private fun hideAlert() {
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_in_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.GONE
    }
}