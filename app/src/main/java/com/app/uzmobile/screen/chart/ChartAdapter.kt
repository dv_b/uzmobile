package com.app.uzmobile.screen.chart

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.uzmobile.R
import com.app.uzmobile.content.Detail
import com.app.uzmobile.content.Unused
import java.text.SimpleDateFormat
import java.util.*

class ChartAdapter(context: Context) : RecyclerView.Adapter<ChartAdapter.ViewHolder>() {

    var list = ArrayList<Detail>()
    var mContext = context

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.name?.text = list.get(position).offeringName
        holder?.value?.text = String.format("%.1f", list.get(position).unused)
        holder?.period?.text =  mContext.getString(R.string.til,
                SimpleDateFormat("dd.MM.yyyy").format( Date(list.get(position).expire!! * 1000)))
    }

    private fun getSum(vals: List<Unused>?): Float {
        var sum: Float = 0.0f
        for (unused in vals!!) {
            sum = sum.plus(unused.value?.toFloat() )
        }
        return sum
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ChartAdapter.ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.item_chartpack, parent, false))
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var name = itemView?.findViewById<TextView>(R.id.pack_name)
        var value = itemView?.findViewById<TextView>(R.id.pack_value)
        val period = itemView?.findViewById<TextView>(R.id.pack_period)
    }
}

fun  Float.plus(toFloat: Float?): Float {
    return this + toFloat!!
}

