package com.app.uzmobile.screen.packs

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.MainActivity
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.persistant.RealmService
import com.app.uzmobile.screen.packdetails.PackDetailsActivity
import com.app.uzmobile.screen.rates.adapters.AdapterCalback
import com.app.uzmobile.screen.rates.adapters.RatesAdapter
import com.app.uzmobile.utils.RealmConvertor
import io.realm.Realm
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*


class PacksActivity : BaseActivity(), AdapterCalback {

    override fun call(item: Service, position: Int) {
//        subscribePack("Add", item.offeringId)
        val intent = Intent(this, PackDetailsActivity::class.java)
        intent.putExtra(PackDetailsActivity.PACK, item)
        startActivity(intent)
    }

    var tab: TabLayout? = null

    var pager: ViewPager? = null

    var list: RecyclerView? = null

    var items: List<String>? = null

    var adapter: RatesAdapter? = null

    var backButton: ImageView? = null

    var title: TextView? = null

    var emptyList: LinearLayout? = null

    var minutesFragment: MinutesPackFragment? = null

    var internetFragment: InternetPackFragment? = null

    var smsFragment: SMSPackFragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acrivity_packs)

        adapter = RatesAdapter(this, ArrayList<Service>())
        adapter?.callback = this

        title = findViewById(R.id.title)
        val typeFace = Typeface.createFromAsset(assets, "HelveticaNeue.ttf")
        title?.typeface = typeFace
        title?.text = getString(R.string.packs)

        emptyList = findViewById(R.id.empty_list)

        tab = findViewById(R.id.tab)

        pager = findViewById(R.id.pager)
        var pagerAdapter = PacksPagerAdapter(supportFragmentManager)
        minutesFragment = MinutesPackFragment()

        internetFragment = InternetPackFragment()

        smsFragment = SMSPackFragment()

        pagerAdapter.addItem(minutesFragment!!, getString(R.string.minutes_title))
        pagerAdapter.addItem(internetFragment!!, getString(R.string.internet_title))
        pagerAdapter.addItem(smsFragment!!, getString(R.string.sms_title))
        pager?.adapter = pagerAdapter

        tab?.setupWithViewPager(pager)

        val manager = LinearLayoutManager(this)

//        list = findViewById(R.id.list)
//        list?.addItemDecoration(SpaceItemDecorator(Utils.dp2px(resources, 2.0F).toInt()))
//        list?.layoutManager = manager
//        list?.addItemDecoration(SimpleDividerItemDecoration(this))
//        list?.adapter = adapter
//        adapter?.setItems(getMockPacks())
//
        soRequest()

        backButton = findViewById(R.id.menu)
        backButton?.setOnClickListener { _ -> finish() }


    }

    private fun  getMockPacks(): List<String> {
        return listOf("50 мб", "150 мб", "300 мб", "600 мб", "1200 мб", "2400 мб", "6000 мб", "12000 мб", "24000 мб")
    }


    private fun soRequest(action: String = "P_List", offeringId: String? = null) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.SO.name, null, action, offeringId, getPrefs().getString(C.LANG, "ru")))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.soRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map { res ->
                    val err = res.response?.error
                    if (err?.equals("6.1")!!) {
                        getPrefs().edit().clear().apply()
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                    res.response?.query?.items!!
                }
                .subscribe(this::handleServices, { _ ->

                    val result = Realm.getDefaultInstance().where(RealmService::class.java).findAll()
                    if (result.isEmpty()) {
                        setEmptyListVisibility()
                    } else {
                        handleServices(RealmConvertor.getServices(result))
                    }
                    })
    }

    private fun handleServices(items : List<Service>) {
        minutesFragment?.adapter?.callback = this
        internetFragment?.adapter?.callback = this
        smsFragment?.callback = this
        val sms = items.filter { predicate -> predicate.unitType?.equals("SMS", true)!! }.sortedWith(Service)
        minutesFragment?.setData(items.filter { predicate -> predicate.unitType?.equals("voice", true)!! }.sortedWith(Service))
        internetFragment?.setData(items.filter { predicate -> predicate.unitType?.equals("gprs", true)!! }.sortedWith(Service))
        smsFragment?.setData(sms)


        setListVisibility()
        saveCache(items)
//        adapter?.setItems(items.sortedWith(Service))


//        for (i in items) {
////            if (i.name?.contains("MB", ignoreCase = false) as Boolean)
//                adapter?.setItem(i)
//        }
    }



    private fun setEmptyListVisibility() {
        emptyList?.visibility = View.VISIBLE
//        list?.visibility = View.GONE
    }

    private fun setListVisibility() {
        emptyList?.visibility = View.GONE
//        list?.visibility = View.VISIBLE
    }

    private fun  saveCache(items: List<Service>?) {
        Realm.getDefaultInstance().executeTransaction {
            realm -> realm.copyToRealmOrUpdate(RealmConvertor.getRealmServices(items)) }
    }

    private fun subscribePack(action: String = "Add", offeringId: String? = null) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.SO.name, null, action, offeringId))
        val jsend = JSend(request)
        ApiFactory.inPayService!!.soRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleSubscribe, { _ -> Log.d("TAG", "error")})
    }




    private fun handleSubscribe(res : JReceive<List<Service>>) {
        if (res.response?.query?.result?.equals("true") as Boolean)
            Toast.makeText(this, "Вы приобрели пакет!!!", Toast.LENGTH_LONG).show()
        else
            Toast.makeText(this, "Вы не приобрели пакет!!!", Toast.LENGTH_LONG).show()
    }



}