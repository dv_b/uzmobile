package com.app.uzmobile.screen.packdetails

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.uzmobile.R
import com.app.uzmobile.content.Section

class SectionsAdapter(context: Context, list: List<Section>?) : RecyclerView.Adapter<SectionsAdapter.ViewHolder>() {

    var mContext = context
    var items = list


    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.items?.layoutManager = LinearLayoutManager(mContext)
        val adapter = PackDetailsAdapter(mContext, items?.get(position)?.items)
        holder?.items?.adapter = adapter
        holder?.title?.text = items?.get(position)?.name
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return SectionsAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_section, parent, false))
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var items = itemView?.findViewById<RecyclerView>(R.id.items)
        var title = itemView?.findViewById<TextView>(R.id.title)

    }
}

