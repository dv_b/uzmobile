package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName


class SMS {

    @SerializedName("TotalAmt")
    var number: Int? = null

    @SerializedName("Detail")
    var details: List<Detail>? = null

    @SerializedName("Percent")
    var percents: List<String>? = null
}