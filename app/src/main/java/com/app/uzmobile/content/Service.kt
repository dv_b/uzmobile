package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Service : Serializable {

    @SerializedName("OfferingId")
    var offeringId: String? = null

    @SerializedName("Name")
    var name: String? = null

    @SerializedName("MonthlyCost")
    var monthlyCost: String? = null

    @SerializedName("OneTimeCost")
    var oneTimeCost: String? = null

    @SerializedName("ExpireDateTimeshtamp")
    var expireDateTimeshtamp: Long? = null

    @SerializedName("ExpireDate")
    var expireDate: Long? = null

    @SerializedName("PaymentOnAdd")
    var paymentOnAdd: Int? = null

    @SerializedName("UnitType")
    var unitType: String? = null

    var isChecked = false

    companion object : Comparator<Service> {

        override fun compare(a: Service, b: Service): Int {
            if (a.monthlyCost != null && b.monthlyCost != null) {
                return (a.monthlyCost!!.toDouble() - b.monthlyCost!!.toDouble()).toInt()
            } else return 0
        }


    }
}