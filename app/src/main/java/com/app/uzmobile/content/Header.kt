package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName


class Header {
    var id: String? = System.currentTimeMillis().toString()
    @SerializedName("Token")
    var token: String? = null
    var App = App()
    var Account:Account? = com.app.uzmobile.content.Account()
}

