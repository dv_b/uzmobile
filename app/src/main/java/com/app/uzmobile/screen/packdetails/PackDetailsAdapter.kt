package com.app.uzmobile.screen.packdetails

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.app.uzmobile.R
import com.app.uzmobile.content.Item
import com.squareup.picasso.Picasso

class PackDetailsAdapter(context: Context, items: List<Item>?): RecyclerView.Adapter<PackDetailsAdapter.ViewHolder>() {

    var items = items

    var mContext = context

    override fun getItemViewType(position: Int): Int {
        return items?.get(position)?.itemId?.toInt()!!
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        if (viewType == 1001) {
            return PackDetailsAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.icon_text_item, parent, false))

        } else if (viewType == 1002 || viewType == 2002) {
            return PackDetailsAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.text_item, parent, false))
        }
        return PackDetailsAdapter.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.icon_text_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        if (holder?.icon != null)
        Picasso.with(mContext)
                .load(items?.get(position)?.params?.iconName)
//                .placeholder(R.drawable.user_placeholder)
//                .error(R.drawable.user_placeholder_error)
                .into(holder?.icon)
        holder?.name?.text = items?.get(position)?.params?.itemName
        holder?.value?.text = items?.get(position)?.params?.value
        holder?.text?.text = items?.get(position)?.params?.texts
    }

    private fun putText(text: TextView?, texts: List<String>?) {
        var str = StringBuilder()

//        var i = 1
        if (texts != null) {
            for (t in texts) {
                str.append(t + System.getProperty("line.separator") + System.getProperty("line.separator"))
//                i += 1
            }
        }
        text?.text = str.toString()

    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var icon = itemView?.findViewById<ImageView>(R.id.ic_internet)
        var name = itemView?.findViewById<TextView>(R.id.text_internet)
        var value = itemView?.findViewById<TextView>(R.id.internet_payed)
        var text = itemView?.findViewById<TextView>(R.id.text)
    }
}
