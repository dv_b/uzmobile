package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName


class JReceive<T> {
    @SerializedName("Response")
    var response: BaseResponse<T>? = null
}