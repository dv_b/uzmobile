package com.app.uzmobile.screen.general;

public interface LoadingView {

    void showLoading();

    void hideLoading();

}