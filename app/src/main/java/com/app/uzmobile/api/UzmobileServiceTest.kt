package com.app.uzmobile.api

import com.app.uzmobile.content.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable


interface UzmobileServiceTest {

    @POST("SharePoint/UzMobileAppDev.php/")
    fun balanceRequest(@Body request: JSend): Observable<JReceive<String>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun freeUnitRequest(@Body request: JSend): Observable<JReceive<List<Packet>>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun totalFreeUnitsRequest(@Body request: BaseRequest): Observable<BaseResponse<String>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun percentFreeUnits(@Body request: JSend): Observable<JReceive<PercentFreeUnit>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun currentTarif(@Body request: JSend): Observable<JReceive<Tarif>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun soRequest(@Body request: JSend): Observable<JReceive<List<Service>>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun serviceSwitch(@Body request: JSend): Observable<JReceive<Any>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun sessionsRequest(@Body request: JSend): Observable<JReceive<MutableList<Session>>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun deleteSession(@Body request: JSend): Observable<JReceive<String>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun changeLang(@Body request: JSend): Observable<JReceive<String>>

    @POST("SharePoint/UzMobileAppDev.php/")
    fun getService(@Body request: JSend): Observable<JReceive<Tarifs>>


    @GET("CRM/ManageServices/pakets/AppPacketInfo.php")
    fun request(@Query("OfferingId") offeringId: String, @Query("Lang") lang: String = "ru"): Observable<Tarifs>

    @GET("CRM/ManageServices/uslugi/AppUslugiInfo.php")
    fun services(@Query("OfferingId") offeringId: String, @Query("Lang") lang: String = "ru"): Observable<Tarifs>

    @GET("CRM/ManageServices/tarifs/AppTarifInfo.php")
    fun tarifs(@Query("OfferingId") offeringId: String, @Query("Lang") lang: String = "ru"): Observable<Tarifs>

}