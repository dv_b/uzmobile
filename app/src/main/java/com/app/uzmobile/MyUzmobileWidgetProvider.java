package com.app.uzmobile;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.widget.RemoteViews;

import com.app.uzmobile.services.UpdateService;

public class MyUzmobileWidgetProvider extends AppWidgetProvider {

  private PendingIntent service;

  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
    final AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    final Intent i = new Intent(context, UpdateService.class);

    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
      context.startForegroundService(i);
    } else {
      context.startService(i);
    }

    if (service == null) {
      service = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    RemoteViews view = new RemoteViews(context.getPackageName(), R.layout.widget_view);
    view.setOnClickPendingIntent(R.id.update, service);
//    view.setTextViewText(R.id.bal_lab, context.getString(R.string.balance_));

    appWidgetManager.updateAppWidget(appWidgetIds, view);

    if (manager != null) {
      manager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), 60000, service);
    }


  }
}
