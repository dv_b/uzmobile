package com.app.uzmobile.screen.services

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.app.uzmobile.R
import com.app.uzmobile.content.Service
import com.app.uzmobile.screen.rates.adapters.AdapterCalback
import com.app.uzmobile.screen.servicedetails.ServiceDetailsActivity

class ServiceAdapter(context: Context, items: MutableList<Service>) : RecyclerView.Adapter<ServiceAdapter.ViewHolder>() {

    var list: MutableList<Service> = items
    var mContext = context

    val helveticaLight = Typeface.createFromAsset(context.assets, "HelveticaNeue.ttf")

    var callback: AdapterCalback? = null
        set(value) {
            field = value
        }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(items: List<Service>) {
        list.addAll(items)
        notifyDataSetChanged()
    }

    fun setItem(item: Service) {
        list.add(item)
        notifyDataSetChanged()
    }



    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val item = list.get(position)

        holder?.name?.text = item.name

        if (!item.isChecked) {
            holder?.status?.text = mContext.getString(R.string.service_not_active)
            holder?.status?.setTextColor(ContextCompat.getColor(mContext, R.color.status_off))
        }

//        holder?.sumscribe?.isChecked = list.get(position).isChecked

//        holder?.itemView?.setOnClickListener { _ -> showDetails(position) }
//        holder?.sumscribe?.setOnCheckedChangeListener { view, isChecked ->
//            if (isChecked) {
//            showDetails(position)
////            holder.sumscribe?.isChecked = true
////            view.toggle()
////            view.isEnabled = false
//        } else {
////            holder.sumscribe?.isChecked = false
////            view.toggle()
////            view.isEnabled = true
//        }}

        holder?.itemView?.setOnClickListener({view ->
            val intent = Intent(mContext, ServiceDetailsActivity::class.java)
            intent.putExtra(ServiceDetailsActivity.PACK, list.get(position))
            mContext.startActivity(intent)
        })
    }

    fun setChecked(check: Boolean, position: Int) {
        list.get(position).isChecked = check
        notifyItemChanged(position)
    }

    private fun  showDetails(position: Int) {
        callback?.call(list[position], position)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_service, parent, false))
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var name = itemView?.findViewById<TextView>(R.id.name)
//        var sumscribe = itemView?.findViewById<com.suke.widget.SwitchButton>(R.id.trigger)

        var status = itemView?.findViewById<TextView>(R.id.status)
        init {
            name?.typeface = helveticaLight
        }

    }


}