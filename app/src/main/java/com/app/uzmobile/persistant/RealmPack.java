package com.app.uzmobile.persistant;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class RealmPack extends RealmObject {
    @PrimaryKey
    public String offeringId;

    public String name;

    public String monthlyCost;

    public String oneTimeCost;

    public Long expireDateTimeshtamp;

    public Long expireDate;

    public Integer paymentOnAdd;

    public boolean isChecked;

}