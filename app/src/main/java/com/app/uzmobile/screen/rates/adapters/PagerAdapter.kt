package com.app.uzmobile.screen.rates.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter


class PagerAdapter(fm: FragmentManager, list: List<Fragment>) : FragmentStatePagerAdapter(fm) {

    var items: List<Fragment> = list

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): Fragment {
        if (position == 0) {
            return items.get(0)
        } else if (position == 1) {
            return items.get(1)
        } else
            return items.get(0)
    }
}