package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName

class BaseResponse<T> {
    var id: String? = null

    @SerializedName("Error")
    var error: String? = null

    @SerializedName("Message")
    var message: String? = null

    @SerializedName("ProcessID")
    var processId: String? = null

    @SerializedName("Query")
    var query: Query<T>? = null
}

