package com.app.uzmobile.services;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.app.uzmobile.C;
import com.app.uzmobile.MyUzmobileWidgetProvider;
import com.app.uzmobile.R;
import com.app.uzmobile.api.ApiFactory;
import com.app.uzmobile.content.BaseRequest;
import com.app.uzmobile.content.Header;
import com.app.uzmobile.content.JReceive;
import com.app.uzmobile.content.JSend;
import com.app.uzmobile.content.PercentFreeUnit;
import com.app.uzmobile.content.Query;
import com.app.uzmobile.content.RequestType;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UpdateService extends Service {


  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public void onCreate() {
    super.onCreate();

    if (Build.VERSION.SDK_INT >= 26) {
      String CHANNEL_ID = "my_channel_01";
      NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
          "Channel human readable title",
          NotificationManager.IMPORTANCE_DEFAULT);

      ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

      Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
          .setContentTitle("")
          .setContentText("").build();

      startForeground(1, notification);
    }
    try {
      ApiFactory.INSTANCE.recreate();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    // generates random number

   initRequest();
   percentFreeUnitRequest();




    return super.onStartCommand(intent, flags, startId);
  }

  private void initRequest() {
    startLoader();
    Header header = new Header();
    header.setToken(getSharedPreferences(C.INSTANCE.getPREFS(), MODE_PRIVATE).getString(C.INSTANCE.getTOKEN(), "")); //= getPrefs().getString(C.TOKEN, "")
//        header.Account.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account.Pass = getPrefs().getString(C.PASSWORD, "")
    BaseRequest request = new BaseRequest (header, new Query(null, RequestType.Balance.name(), null, null, null, null, null));
    JSend jsend = new JSend(request);
    ApiFactory.INSTANCE.getInPayService().balanceRequest(jsend)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::handleBalance, throwable -> {
          Log.d("TAG", "error balance");
          stopLoader();
          hideContent();
        });

  }

  private void handleBalance(JReceive<String> res) {
    boolean ok = res.getResponse().getQuery().getType().equals("sendNewPass");
    if (ok) {
      hideContent();
      return;
    }

    String err = res.getResponse().getError();
    if (err != null && err.equals("6.1")) {
      hideContent();
      return;
    }

    stopLoader();
    showContent();

    RemoteViews view = new RemoteViews(getPackageName(), R.layout.widget_view);

    Date d = new Date(System.currentTimeMillis());
    DateFormat f = new SimpleDateFormat("в HH:mm");
    view.setTextViewText(R.id.time, f.format(d));

    view.setTextViewText(R.id.balance, getString(R.string.balance_val, res.getResponse().getQuery().getBalance().getUzs() + ""));
    ComponentName theWidget = new ComponentName(this, MyUzmobileWidgetProvider.class);
    AppWidgetManager manager = AppWidgetManager.getInstance(this);
    manager.updateAppWidget(theWidget, view);
  }

  private void percentFreeUnitRequest() {
    Header header = new Header();
    header.setToken(getSharedPreferences(C.INSTANCE.getPREFS(), MODE_PRIVATE).getString(C.INSTANCE.getTOKEN(), ""));
    header.setAccount(null);
//        header.Account.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account.Pass = getPrefs().getString(C.PASSWORD, "")
    BaseRequest request = new BaseRequest(header, new Query(null, RequestType.PercentFreeUnit.name(), null, null, null, null, null));
    JSend jsend = new JSend(request);
    ApiFactory.INSTANCE.getSDevTest().percentFreeUnits(jsend)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(this::handlePercentFreeUnit, throwable -> {
          Log.d("TAG", "error");
          stopLoader();
          hideContent();
        });
  }

  private void handlePercentFreeUnit(JReceive<PercentFreeUnit> res) {

    boolean ok = res.getResponse().getQuery().getType().equals("sendNewPass");
    if (ok) {
      hideContent();
      return;
    }

    String err = res.getResponse().getError();
    if (err != null && err.equals("6.1")) {
      hideContent();
      return;
    }

    stopLoader();
    showContent();


    int voiceAmount = res.getResponse().getQuery().getItems().getVoice().getTotalAmount();
    int vMinutes = voiceAmount / 60;

    RemoteViews view = new RemoteViews(getPackageName(), R.layout.widget_view);
    view.setTextViewText(R.id.voice, getString(R.string.miutes_wid, vMinutes));

    int gprsAmount = res.getResponse().getQuery().getItems().getGprs().getTotalAmount();
    int mBytes = gprsAmount / 1024;

    view.setTextViewText(R.id.gprs, getString(R.string.mbytes_wid, mBytes));


    int smsAmount = res.getResponse().getQuery().getItems().getSms().getTotalAmount();

    view.setTextViewText(R.id.messages, getString(R.string.sms_int, smsAmount));

    updateView(view);


  }

  private void stopLoader() {
    RemoteViews view = new RemoteViews(getPackageName(), R.layout.widget_view);
    view.setViewVisibility(R.id.loader, View.GONE);
    view.setViewVisibility(R.id.update, View.VISIBLE);
    ComponentName theWidget = new ComponentName(this, MyUzmobileWidgetProvider.class);
    AppWidgetManager manager = AppWidgetManager.getInstance(this);
    manager.updateAppWidget(theWidget, view);
    updateView(view);
  }

  private void startLoader() {
    RemoteViews view = new RemoteViews(getPackageName(), R.layout.widget_view);
    view.setViewVisibility(R.id.loader, View.VISIBLE);
    view.setViewVisibility(R.id.update, View.INVISIBLE);
    updateView(view);
  }

  private void showContent() {
    RemoteViews view = new RemoteViews(getPackageName(), R.layout.widget_view);
    view.setViewVisibility(R.id.content, View.VISIBLE);
    view.setViewVisibility(R.id.balance, View.VISIBLE);
    view.setViewVisibility(R.id.wallet, View.VISIBLE);
    view.setViewVisibility(R.id.update_container, View.VISIBLE);
    view.setViewVisibility(R.id.error_massega, View.GONE);
    updateView(view);
  }

  private void hideContent() {
    RemoteViews view = new RemoteViews(getPackageName(), R.layout.widget_view);
    view.setViewVisibility(R.id.content, View.GONE);
    view.setViewVisibility(R.id.balance, View.GONE);
    view.setViewVisibility(R.id.wallet, View.GONE);
    view.setViewVisibility(R.id.update_container, View.GONE);
    view.setViewVisibility(R.id.error_massega, View.VISIBLE);
    updateView(view);
  }

  private void updateView(RemoteViews view) {
    ComponentName theWidget = new ComponentName(this, MyUzmobileWidgetProvider.class);
    AppWidgetManager manager = AppWidgetManager.getInstance(this);
    manager.updateAppWidget(theWidget, view);
  }


}
