package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName

class ServiceSwitch {
    @SerializedName("Type")
    var type: String? = "ServiceOffering"

    @SerializedName("Action")
    var action: String? = null

    @SerializedName("Result")
    var result: String? = null

    @SerializedName("OfferingId")
    var offeringId: String? = null
}

