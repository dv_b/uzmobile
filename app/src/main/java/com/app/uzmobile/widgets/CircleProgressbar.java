package com.app.uzmobile.widgets;


import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.app.uzmobile.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CircleProgressbar extends View {


  /**
   * ProgressBar's line thickness
   */
  private float strokeWidth = 4;
  private float progress = 0;
  private List<Float> progressList = new ArrayList<>();
  private int min = 0;
  private int max = 100;
  /**
   * Start the progress at 12 o'clock
   */
  private int startAngle = -90;
  private int color = Color.DKGRAY;
  private RectF rectF;
  private Paint backgroundPaint;
  private Paint foregroundPaint;
  private Paint secondPaint;
  private Paint thirdPaint;
  private Paint fourthPaint;
  private Paint fifthPaint;

  Paint dynamicColor;

  private Paint[] colors = new Paint[5];

  private int tempAngle = -90;

  private int colorNum = 0;

  public float getStrokeWidth() {
    return strokeWidth;
  }

  public void setStrokeWidth(float strokeWidth) {
    this.strokeWidth = strokeWidth;
    backgroundPaint.setStrokeWidth(strokeWidth);
    foregroundPaint.setStrokeWidth(strokeWidth);
    invalidate();
    requestLayout();//Because it should recalculate its bounds
  }

  public float getProgress() {
    return progress;
  }

  public void setProgress(float progress) {
    this.progress = progress;
    invalidate();
  }

  public int getMin() {
    return min;
  }

  public void setMin(int min) {
    this.min = min;
    invalidate();
  }

  public int getMax() {
    return max;
  }

  public void setMax(int max) {
    this.max = max;
    invalidate();
  }

  public int getColor() {
    return color;
  }

  public void setColor(int color) {
    this.color = color;
//    backgroundPaint.setColor(adjustAlpha(color, 0.3f));
    backgroundPaint.setColor(ContextCompat.getColor(getContext(), R.color.percent_bg_stroke));
//    for (int i = 0; i < 5; i++) {
//      colors[i].setColor(adjustAlpha(color, 1 / (i + 1)));
//    }

    colors[0].setColor(this.color);
    dynamicColor.setColor(this.color);
//    colors[1].setColor(Color.CYAN);
//    colors[2].setColor(Color.BLUE);
//    colors[3].setColor(Color.RED);
//    colors[4].setColor(Color.MAGENTA);

    invalidate();
    requestLayout();
  }

  public void setSecondColor(int color) {
    colors[1].setColor(color);

    invalidate();
    requestLayout();
  }

  public void setThirdColor(int color) {
    colors[2].setColor(color);

    invalidate();
    requestLayout();
  }

  public void setFourthColor(int color) {
    colors[3].setColor(color);

    invalidate();
    requestLayout();
  }

  public void setFifthColor(int color) {
    colors[4].setColor(color);

    invalidate();
    requestLayout();
  }



  public CircleProgressbar(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context, attrs);
  }

  private void init(Context context, AttributeSet attrs) {
    rectF = new RectF();
    TypedArray typedArray = context.getTheme().obtainStyledAttributes(
        attrs,
        R.styleable.CircleProgressBar,
        0, 0);
    //Reading values from the XML layout
    try {
      strokeWidth = typedArray.getDimension(R.styleable.CircleProgressBar_progressBarThickness, strokeWidth);
      progress = typedArray.getFloat(R.styleable.CircleProgressBar_progress, progress);
      tempAngle = typedArray.getInt(R.styleable.CircleProgressBar_tempAngle, tempAngle);
      color = typedArray.getInt(R.styleable.CircleProgressBar_progressbarColor, color);
      min = typedArray.getInt(R.styleable.CircleProgressBar_min, min);
      max = typedArray.getInt(R.styleable.CircleProgressBar_max, max);
    } finally {
      typedArray.recycle();
    }

    backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//    backgroundPaint.setColor(adjustAlpha(color, 0.3f));
    backgroundPaint.setColor(ContextCompat.getColor(getContext(), R.color.percent_bg_stroke));
    backgroundPaint.setStyle(Paint.Style.STROKE);
    backgroundPaint.setStrokeWidth(strokeWidth);


    for (int i = 0; i < 5; i++) {
      colors[i] = new Paint(Paint.ANTI_ALIAS_FLAG);
//      colors[i].setColor(color);
//      colors[i].setColor(adjustAlpha(color, 1 / (i + 1.0f)));
      colors[i].setStyle(Paint.Style.STROKE);
      colors[i].setStrokeWidth(strokeWidth);
    }

    dynamicColor = new Paint(Paint.ANTI_ALIAS_FLAG);
    dynamicColor.setStyle(Paint.Style.STROKE);
    dynamicColor.setStrokeWidth(strokeWidth);

    colors[1].setColor(Color.CYAN);
    colors[2].setColor(Color.BLUE);
    colors[3].setColor(Color.RED);
    colors[4].setColor(Color.MAGENTA);

//    colors[1].setColor(ContextCompat.getColor(getContext(), R.color.sms_second_pack));

  }

  private int getRand() {
    Random r = new Random();
    int Low = 0;
    int High = 5;
    return r.nextInt(High-Low) + Low;
  }



  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    canvas.drawOval(rectF, backgroundPaint);

    tempAngle = startAngle;


    for (int i = 0; i < progressList.size(); i++) {
      float angle = 360 * progressList.get(i) / max;
      canvas.drawArc(rectF, tempAngle, angle, false, colors[i % 5]);
      tempAngle += (int) angle;

    }





//    for (int i = 0; i < progressList.size(); i++) {
////      Log.i("PROGRESS", progress + "");
//      if (progress <= progressList.get(i)) {
//        dynamicColor = colors[i % 5];
//        if (i > 0){
//          float borderAngle = 360 * progressList.get(i - 1) / max;
//          startAngle = (int) borderAngle - 90;
//        }
//
//        break;
//      }
//    }
//
//    float angle = 360 * progress / max;
//    canvas.drawArc(rectF, startAngle, angle, false, dynamicColor);

  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

    final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
    final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
    final int min = Math.min(width, height);
    setMeasuredDimension(min, min);
    rectF.set(0 + strokeWidth / 2, 0 + strokeWidth / 2, min - strokeWidth / 2, min - strokeWidth / 2);
  }

  /**
   * Lighten the given color by the factor
   *
   * @param color  The color to lighten
   * @param factor 0 to 4
   * @return A brighter color
   */
  public int lightenColor(int color, float factor) {
    float r = Color.red(color) * factor;
    float g = Color.green(color) * factor;
    float b = Color.blue(color) * factor;
    int ir = Math.min(255, (int) r);
    int ig = Math.min(255, (int) g);
    int ib = Math.min(255, (int) b);
    int ia = Color.alpha(color);
    return (Color.argb(ia, ir, ig, ib));
  }

  /**
   * Transparent the given color by the factor
   * The more the factor closer to zero the more the color gets transparent
   *
   * @param color  The color to transparent
   * @param factor 1.0f to 0.0f
   * @return int - A transplanted color
   */
  public int adjustAlpha(int color, float factor) {
    int alpha = Math.round(Color.alpha(color) * factor);
    int red = Math.round(Color.red(color) * factor);
    int green = Math.round(Color.green(color) * factor);
    int blue = Math.round(Color.blue(color) * factor);
    return Color.argb(alpha, red, green, blue);
  }

  /**
   * Set the progress with an animation.
   * Note that the {@link android.animation.ObjectAnimator} Class automatically set the progress
   * so don't call the {@link CircleProgressbar#setProgress(float)} directly within this method.
   *
   * @param progress The progress it should animate to it.
   */
  public void setProgressWithAnimation(float progress) {

    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "progress", progress);// + this.progress);
    objectAnimator.setDuration(1500);
    objectAnimator.setInterpolator(new DecelerateInterpolator());
    objectAnimator.start();
    colorNum++;
  }

  public void setAllProgressWithAnimation(List<String> progressList) {
    progress = 0;
    colorNum = 0;
    float sum = 0.0f;
    this.progressList.clear();
    for (String progress : progressList) {
      this.progressList.add(Float.parseFloat(progress));
      setProgressWithAnimation(Float.parseFloat(progress));
//      sum += Float.parseFloat(progress);

    }
//    setProgressWithAnimation(sum);

  }
}

