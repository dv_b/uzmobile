package com.app.uzmobile


import android.app.Application
import com.app.uzmobile.api.ApiFactory
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.rx.RealmObservableFactory

class AppDelegate : Application() {

    override fun onCreate() {
        super.onCreate()
        ApiFactory.recreate()

        Realm.init(this)
        val configuration = RealmConfiguration.Builder()
                .rxFactory(RealmObservableFactory())
                .deleteRealmIfMigrationNeeded() //temporary
                .build()

        Realm.setDefaultConfiguration(configuration)
    }

}
