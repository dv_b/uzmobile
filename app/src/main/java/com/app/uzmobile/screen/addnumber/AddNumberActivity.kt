package com.app.uzmobile.screen.addnumber

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.R
import com.app.uzmobile.content.JReceive
import com.app.uzmobile.screen.general.LoadingDialog
import com.app.uzmobile.screen.general.LoadingView
import com.app.uzmobile.screen.registration.confirm.ConfirmActivity


class AddNumberActivity : BaseActivity() {

    var close: ImageView? = null

    var submit: Button? = null

    var name: TextView? = null

    var number: TextView? = null

    var loadingView: LoadingView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addnumber)

        initView()

    }

    private fun initView() {
        close = findViewById(R.id.back)
        close?.setOnClickListener {  finish() }

        name = findViewById(R.id.name)

        number = findViewById(R.id.number)

        submit = findViewById(R.id.submit)
        submit?.setOnClickListener {  login() }

        loadingView = LoadingDialog.view(supportFragmentManager)
    }

    private fun login() {
        if (isValid()) {
            startSmsConfirmActivity()
//            val header = Header()
//            header.Account?.ServiceNumber = getNumber()
//            val request = BaseRequest (header, Query(null, RequestType.Balance.name, null))
//            val jsend = JSend(request)
//            showLoading()
//            ApiFactory.inPayService!!.balanceRequest(jsend)
//                    .subscribeOn(Schedulers.newThread())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .doOnSubscribe {this::showLoading }
//                    .subscribe(this::handleLogin, {
//                        Log.d("TAG", "error")
//                        loadingView?.hideLoading()
////                        showAlert()
//                    })
        }
    }

    private fun getNumber() : String? {
        val num = number?.text.toString().replace("+998", "", false).replace(" ", "")
        return num
    }


    private fun showLoading() {
        loadingView?.showLoading()
    }

    private fun handleLogin(res : JReceive<String>) {
        val ok = res.response?.error?.equals("1.23")
        if (ok != null && ok) {
            login()
            return
        }
        loadingView?.hideLoading()

        val status = res.response?.error?.equals("0")
        if (status != null && !status) {
//            showAlert()
            return
        }
        if (res.response?.query?.result.equals("true", false)) {
//            getPrefs().edit().putString(C.LOGIN, getNumber()).apply()
            //todo -> show
            startSmsConfirmActivity()
        }
    }

    private fun startSmsConfirmActivity() {
        val intent = Intent(this, ConfirmActivity::class.java)
        intent.putExtra(C.LOGIN, getNumber())
        startActivity(intent)
        finish()
    }



    fun isValid() : Boolean {
        val num = number?.text.toString()
        if (TextUtils.isEmpty(num) || num.length != 13) {
            return false
        }

        if (!num.substring(0, 6).equals("+99899") && !num.substring(0, 6).equals("+99895"))
            return false

        return true
    }

//    private fun showAlert() {
//        val animate = TranslateAnimation(0f, 0f, 0f, (alert?.getHeight()!!.times(1.0)).toFloat())
//        animate.duration = 400
//        animate.fillAfter = true
//        (alert as LinearLayout).startAnimation(animate)
//        (alert as LinearLayout).setVisibility(View.VISIBLE)
//
//        handler.postDelayed(this::hideAlert, 3000)
//
//    }
//
//    private fun hideAlert() {
//        val animate = TranslateAnimation(0f, 0f, (alert?.getHeight()!!.times(1.0)).toFloat(), 0f)
//        animate.duration = 400
//        animate.fillAfter = true
//        (alert as LinearLayout).startAnimation(animate)
//        (alert as LinearLayout).setVisibility(View.GONE)
//    }

}