package com.app.uzmobile.screen.web;


import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.uzmobile.BaseActivity;
import com.app.uzmobile.R;
import com.app.uzmobile.widgets.MyWebViewClient;

import org.jetbrains.annotations.Nullable;

public class WebActivity extends BaseActivity {

  public static final String TITLE = "title";

  public static final String LINK = "link";
  WebView webView;

  TextView title;

  ImageView back;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_web);

    webView = findViewById(R.id.web);

    title = findViewById(R.id.title);
    String name = getIntent().getStringExtra(TITLE);
    if (!TextUtils.isEmpty(name))
      title.setText(name);

    back = findViewById(R.id.menu);
    back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(webView.canGoBack()) {
          webView.goBack();
        } else {
          finish();
        }
      }
    });


    webView.setWebViewClient(new MyWebViewClient());
//    mWebView.getSettings().setJavaScriptEnabled(true);

    webView.loadUrl(getIntent().getStringExtra(LINK));
    webView.getSettings().setUseWideViewPort(true);
    webView.getSettings().setLoadWithOverviewMode(true);
//    webView.getSettings().setSupportZoom(true);
    webView.getSettings().setBuiltInZoomControls(true);
  }

  private int getScale(){
    Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    int width = display.getWidth();

    return width;
  }

  @Override
  public void onBackPressed() {
    if(webView.canGoBack()) {
      webView.goBack();
    } else {
      super.onBackPressed();
    }
  }
}
