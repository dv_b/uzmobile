package com.app.uzmobile.content

class Tarifs {
    var version: Int? = null

    var name: String? = null

    var value: String? = null

    var sections: List<Section>? = null
}
