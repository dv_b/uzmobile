package com.app.uzmobile

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.app.uzmobile.screen.general.LoadingDialog
import com.app.uzmobile.screen.general.LoadingView
import com.app.uzmobile.screen.registration.confirm.ConfirmActivity
import com.google.firebase.crash.FirebaseCrash


class MainActivity : BaseActivity() {

    var login: EditText? = null
    var submit: Button? = null
    var alert: LinearLayout? = null

    var loadingView: LoadingView? = null

    var handler: Handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseCrash.report(Exception("My first Android non-fatal error"))

        login = findViewById(R.id.login)
        login?.setSelection(4)

        alert = findViewById(R.id.alert)

        submit = findViewById(R.id.submit)
        submit?.setOnClickListener { _ ->  startSmsConfirmActivity()}

        loadingView = LoadingDialog.view(supportFragmentManager)

    }


    private fun getNumber() : String? {
        val num = login?.text.toString().replace("+998", "", false).replace(" ", "")
        return num
    }

    private fun showLoading() {
        loadingView?.showLoading()
    }

    private fun startSmsConfirmActivity() {
        if (isValid()) {
            val intent = Intent(this, ConfirmActivity::class.java)
            intent.putExtra(C.LOGIN, getNumber())
            startActivity(intent)
            finish()
        } else {
            showAlert()
        }
    }

    fun isValid() : Boolean {
        val num = login!!.text.toString()
        if (TextUtils.isEmpty(num) || num.length != 13) {
            return false
        }

        if (!num.substring(0, 6).equals("+99899") && !num.substring(0, 6).equals("+99895"))
            return false

        return true
    }

    fun isTestNum(): Boolean {
        val num = login!!.text.toString()

        if (num.equals("+998998876901") || num.equals("+998998876903")
                || num.equals("+998998876904") || num.equals("+998998876905")
                || num.equals("+998998876908") || num.equals("+998998765366")
                || num.equals("+998998755366") || num.equals("+998998485366")
                || num.equals("+998998695366") || num.equals("+998998405366")
                || num.equals("+998998667377") || num.equals("+998998550099")
                || num.equals("+998998446556") || num.equals("+998998558055")
                || num.equals("+998998201402") || num.equals("+998998119888"))
            return true

        return false
    }

    private fun showAlert() {
        val animate = TranslateAnimation(0f, 0f, 0f, (alert?.getHeight()!!.times(1.0)).toFloat())
        animate.duration = 400
        animate.fillAfter = true
        (alert as LinearLayout).startAnimation(animate)
        (alert as LinearLayout).setVisibility(View.VISIBLE)

        handler.postDelayed(this::hideAlert, 3000)

    }

    private fun hideAlert() {
        val animate = TranslateAnimation(0f, 0f, (alert?.getHeight()!!.times(1.0)).toFloat(), 0f)
        animate.duration = 400
        animate.fillAfter = true
        (alert as LinearLayout).startAnimation(animate)
        (alert as LinearLayout).setVisibility(View.GONE)
    }


}
