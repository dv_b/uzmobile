package com.app.uzmobile.screen.services

import android.app.AlertDialog
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.persistant.RealmPack
import com.app.uzmobile.screen.general.LoadingDialog
import com.app.uzmobile.screen.general.LoadingView
import com.app.uzmobile.screen.rates.SpaceItemDecorator
import com.app.uzmobile.screen.rates.adapters.AdapterCalback
import com.app.uzmobile.screen.rates.adapters.RatesAdapter
import com.app.uzmobile.utils.RealmConvertor
import com.app.uzmobile.widgets.SimpleDividerItemDecoration
import com.github.lzyzsd.circleprogress.Utils
import io.realm.Realm
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers




class ServicesActivity : BaseActivity(), AdapterCalback {

    override fun call(item: Service, position: Int) {
//        subscribePack("Add", item.offeringId)
//        val intent = Intent(this, ServiceDetailsActivity::class.java)
//        intent.putExtra(ServiceDetailsActivity.PACK, item)
//        startActivity(intent)

        createAndShowAlertDialog(item.offeringId, position)
//        subscribe(item?.offeringId)
    }

    var list: RecyclerView? = null

    var items: List<String>? = null

    var adapter: RatesAdapter? = null

    var serviceAdapter: ServiceAdapter? = null

    var backButton: ImageView? = null

    var title: TextView? = null

    var alert: RelativeLayout? = null

    var errorMessage: TextView? = null

    var successIcon: ImageView? = null

    var loader: LoadingView? = null

    var emptyList: LinearLayout? = null

    var handler: Handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_rates)

        loader = LoadingDialog.view(supportFragmentManager)

        backButton = findViewById(R.id.menu)
        backButton?.setOnClickListener { _ -> finish() }

        adapter = RatesAdapter(this, ArrayList<Service>())
        adapter?.callback = this

        serviceAdapter = ServiceAdapter(this, ArrayList<Service>())
        serviceAdapter?.callback = this

        title = findViewById(R.id.title)
        val typeFace = Typeface.createFromAsset(assets, "HelveticaNeue.ttf")
        title?.typeface = typeFace
        title?.text = getString(R.string.services)

        emptyList = findViewById(R.id.empty_list)

//        tab = findViewById(R.id.tab) as TabLayout?
//
//        pager = findViewById(R.id.pager) as ViewPager?
//        var pagerAdapter = PacksPagerAdapter(supportFragmentManager)
//        pagerAdapter.addItem(MinutesPackFragment(), getString(R.string.minutes_title))
//        pagerAdapter.addItem(InternetPackFragment(), getString(R.string.internet_title))
//        pagerAdapter.addItem(SMSPackFragment(), getString(R.string.sms_title))
//        pager?.adapter = pagerAdapter
//
//        tab?.setupWithViewPager(pager)

        val manager = LinearLayoutManager(this)

        list = findViewById(R.id.list)
        list?.addItemDecoration(SpaceItemDecorator(Utils.dp2px(resources, 2.0F).toInt()))
        list?.layoutManager = manager as RecyclerView.LayoutManager?
        list?.addItemDecoration(SimpleDividerItemDecoration(this))
//        list?.adapter = adapter
        list?.adapter = serviceAdapter
//        adapter?.setItems(getMockPacks())
//

        alert = findViewById(R.id.alert)
        errorMessage = findViewById(R.id.alertTitle)

//        soRequest()
        request()


    }

    private fun createAndShowAlertDialog(offeringId: String?, position: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.warning_purchace_service))
        builder.setPositiveButton(R.string.yes, { dialog, id ->
            //TODO
            subscribe(offeringId, position)
            dialog.dismiss()
        })
        builder.setNegativeButton(R.string.no, { dialog, id ->
            //TODO
            serviceAdapter?.setChecked(false, position)
            dialog.dismiss()
        })
        val dialog = builder.create()
        dialog.show()
    }



    private fun soRequest(action: String = "U_List", offeringId: String? = null) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.SO.name, null, action, offeringId))
        val jsend = JSend(request)
        ApiFactory.inPayService!!.soRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loader?.showLoading() }
                .doOnTerminate { loader?.hideLoading() }
                .map { res -> res.response?.query?.items!! }
                .subscribe(this::handleServices, { _ -> Log.d("TAG", "error")})
    }

    private fun request(action: String = "U_List", offeringId: String? = null) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.SO.name, null, action, offeringId, getPrefs().getString(C.LANG, "ru")))
        val jsend = JSend(request)
        val all = ApiFactory.sDevTest!!
                .soRequest(jsend)
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe { loader?.showLoading() }
//                .doOnTerminate { loader?.hideLoading() }
                .map { res -> res.response?.query?.items!! }
//                .subscribe(this::handleServices, { _ -> Log.d("TAG", "error")})


        val request2 = BaseRequest (header, Query(null, RequestType.PurchasedSO.name, null))
        val jsend2 = JSend(request2)


        val purchased = ApiFactory.sDevTest!!.soRequest(jsend2).map { res -> res.response?.query?.items!! }

        Observable.combineLatest(all, purchased, { t1, t2 ->  combine(t1, t2)})
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loader?.showLoading() }
                .doOnTerminate { loader?.hideLoading() }
                .subscribe(this::handleServices, { _ ->
                    val result = Realm.getDefaultInstance().where(RealmPack::class.java).findAll()
                    if (result.isEmpty()) {
                        setEmptyListVisibility()
                    } else {
                        handleServices(RealmConvertor.getPacks(result))
                    }
                })
    }

    private fun  saveCache(items: List<Service>?) {
        Realm.getDefaultInstance().executeTransaction {
            realm -> realm.copyToRealmOrUpdate(RealmConvertor.getRealmPacks(items)) }
    }

    private fun combine(first: List<Service>, second: List<Service>): List<Service> {
        for (service in first) {
            for (sec in second) {
                if (service.offeringId != null && sec.offeringId != null &&
                        service.offeringId!! == sec.offeringId!!)
                    service.isChecked = true
            }
        }
        return first
    }

    private fun setEmptyListVisibility() {
        emptyList?.visibility = View.VISIBLE
        list?.visibility = View.GONE
    }

    private fun setListVisibility() {
        emptyList?.visibility = View.GONE
        list?.visibility = View.VISIBLE
    }

    private fun subscribe(offeringId: String?, position: Int) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val serviceSwitch = ServiceSwitch()
        serviceSwitch.action = "Start"
        serviceSwitch.offeringId = offeringId
        val request = BaseRequest (header, Query(null, "ServiceOffering", null, "Start", offeringId))
        val jsend = JSend(request)

        ApiFactory.sDevTest!!.serviceSwitch(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loader?.showLoading() }
                .doOnTerminate { loader?.hideLoading() }
                .subscribe({t ->
//                    handleSwitch(t, position)
                }, {t: Throwable? ->  serviceAdapter?.setChecked(false, position)})
    }

    private fun handleSwitch(response: JReceive<ServiceSwitch>, position: Int) {
        val errCode = response.response?.error
        if (errCode != null && errCode != "0") {
            showFailureAlert(response.response?.message)
            serviceAdapter?.setChecked(false, position)
        } else {
            showSuccessAlert()
        }
    }

    private fun unsubscribe(offeringId: String?) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val serviceSwitch = ServiceSwitch()
        serviceSwitch.action = "Stop"
        serviceSwitch.offeringId = offeringId
        val request = BaseRequest (header, Query(serviceSwitch, null, null, null, null))
        val jsend = JSend(request)

        ApiFactory.sDevTest!!.serviceSwitch(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    private fun handleServices(items : List<Service>) {
        setListVisibility()
        saveCache(items)
        for (i in items) {
            serviceAdapter?.setItem(i)
        }
    }

    private fun showSuccessAlert() {
        successIcon?.visibility = View.VISIBLE
        errorMessage?.text = getString(R.string.pack_purchace_success)
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, R.color.sucess))


        handler.postDelayed(this::hideAlert, 3000)
    }

    private fun showFailureAlert(text: String?) {
        successIcon?.visibility = View.GONE
        errorMessage?.text = getString(R.string.err_buy_service)
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_dark))



        handler.postDelayed(this::hideAlert, 3000)

    }


    private fun hideAlert() {
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_in_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.GONE
    }

}