package com.app.uzmobile

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.ButterKnife

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        ButterKnife.bind(this)
    }

    override fun setContentView(view: View?) {
        super.setContentView(view)
        ButterKnife.bind(this)
    }

    override fun setContentView(view: View?, params: ViewGroup.LayoutParams?) {
        super.setContentView(view, params)
        ButterKnife.bind(this)
    }

    fun getPrefs() : SharedPreferences {
        return getSharedPreferences(C.PREFS, Context.MODE_PRIVATE)
    }

    protected fun setHelvetica(view: TextView?) {
        val typeFace = Typeface.createFromAsset(assets, "HelveticaNeue.ttf")
        view?.typeface = typeFace
    }

}


