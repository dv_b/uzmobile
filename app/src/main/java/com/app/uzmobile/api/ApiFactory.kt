package com.app.uzmobile.api

import com.app.uzmobile.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URISyntaxException
import java.util.concurrent.TimeUnit


object ApiFactory {

    private var sClient: OkHttpClient? = null

    @Volatile private var sService: UzmobileService? = null

    @Volatile private var sServiceTest: UzmobileServiceTest? = null



    val sDevTest: UzmobileServiceTest?
        get() {
            var service = sServiceTest
            if (service == null) {
                synchronized(ApiFactory::class) {
                    service = sServiceTest
                    if (service == null) {
                        sServiceTest = buildRetrofit().create(UzmobileServiceTest::class.java)
                        service = sServiceTest
                    }
                }
            }
            return service
        }

    val inPayService: UzmobileService?
        get() {
            var service = sService
            if (service == null) {
                synchronized(ApiFactory::class) {
                    service = sService
                    if (service == null) {
                        sService = buildRetrofit().create(UzmobileService::class.java)
                        service = sService
                    }
                }
            }
            return service
        }


    @Throws(URISyntaxException::class)
    fun recreate() {
        sClient = null
        sClient = client
        sService = buildRetrofit().create(UzmobileService::class.java)
    }

    private fun buildRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
    }


    private val client: OkHttpClient?
        get() {
            var client = sClient
            if (client == null) {
                synchronized(ApiFactory::class) {
                    client = sClient
                    if (client == null) {
                        sClient = buildClient()
                        client = sClient
                    }
                }
            }
            return client
        }

    private fun buildClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                //                .addInterceptor(ApiKeyInterceptor.create())
                .build()
    }

}
