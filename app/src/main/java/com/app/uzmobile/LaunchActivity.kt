package com.app.uzmobile

import android.content.Intent
import android.os.Bundle
import com.app.uzmobile.screen.landing.LandingActivity
import com.app.uzmobile.screen.language.LanguageActivity


class LaunchActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val login = getPrefs().getString(C.LOGIN, "")
        val password = getPrefs().getString(C.PASSWORD, "")
        if (login.equals("", false) || password.equals("", false))
            startLoginActivity()
        else
            startLandingActivity()

    }

    private fun startLoginActivity() {
        startActivity(Intent(this, LanguageActivity::class.java))
        finish()
    }

    private fun startLandingActivity() {
        startActivity(Intent(this, LandingActivity::class.java))
        finish()
    }

}