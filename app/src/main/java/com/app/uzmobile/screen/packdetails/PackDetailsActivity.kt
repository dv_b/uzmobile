package com.app.uzmobile.screen.packdetails

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.screen.general.LoadingDialog
import com.app.uzmobile.screen.general.LoadingView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class PackDetailsActivity : BaseActivity() {

    companion object {
        const val PACK = "pack"
    }

    var backBtn: ImageView? = null

    var pack: TextView? = null

    var packValue: TextView? = null

    var internet: TextView? = null

    var expPeriod: TextView? = null

    var ussdUz: TextView? = null

    var ussdRu: TextView? = null

    var list: RecyclerView? = null

    var addService: Button? = null

    var error: TextView? = null

    var successIcon: ImageView? = null

    var alert: RelativeLayout? = null

    var loadingView: LoadingView? = null

    var handler: Handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packdetail)

        val service = intent.getSerializableExtra(PACK) as Service?

        loadingView = LoadingDialog.view(supportFragmentManager)

        alert = findViewById(R.id.alert)

        error = findViewById(R.id.alertTitle)
        error?.text = getString(R.string.err_buy_pack)

        successIcon = findViewById(R.id.success_ic)

        backBtn = findViewById(R.id.menu)
        backBtn?.setOnClickListener { _ -> finish() }

        addService = findViewById(R.id.add)
        addService?.text = getString(R.string.add_pack)
        addService?.setOnClickListener { _ -> createAndShowAlertDialog(service?.offeringId) }

        pack = findViewById(R.id.pack)
        pack?.text = service?.name

        packValue = findViewById(R.id.balance)
        val balanceVal = service?.monthlyCost?.toFloat()?.times(4210.35f)
        packValue?.text = getString(R.string.balance_val, service?.monthlyCost)

        internet = findViewById(R.id.internet_payed)


        list = findViewById(R.id.list)
        list?.layoutManager = LinearLayoutManager(this)
//        list?.isNestedScrollingEnabled = false
//        list?.setOnTouchListener({view, motionEvent -> true})


//        request(service?.offeringId!!)

        crmRequest(service?.offeringId!!)
    }

    private fun showLoading() {
        loadingView?.showLoading()
    }

    private fun hideLoading() {
        loadingView?.hideLoading()
    }

    private fun request(offeringId: String) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account = null
//        header.Account.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.Service.name, null, "Info", offeringId, null))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.getService(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleTarifs, { t: Throwable? ->   t?.printStackTrace()})
    }

    private fun handleTarifs(res: JReceive<Tarifs>) {
        var adapter = SectionsAdapter(this,res.response?.query?.items?.sections)
        list?.adapter = adapter

    }

    private fun crmRequest(offeringId: String) {
        ApiFactory.sDevTest!!.request(offeringId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    private fun addService(action: String = "Add", offeringId: String? = null) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.SO.name, null, action, offeringId))
        val jsend = JSend(request)
        showLoading()
        ApiFactory.inPayService!!.soRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { this::showLoading }
                .doOnTerminate { this::hideLoading }
//                .map { res -> res.response?.query?.items!! }
                .subscribe(this::handleAddService, { _ ->
                    Log.d("TAG", "error")
                    hideLoading()
                })
    }



    private fun handleAddService(response : JReceive<List<Service>>) {
        hideLoading()
        if (!response.response?.error?.equals("0")!! || response.response?.query?.result?.equals("false")!!) {
//            Toast.makeText(this, "Не удалось, попробуйте позже", Toast.LENGTH_LONG).show()
            showFailureAlert(response.response?.message!!)
//            showSuccessAlert()
        } else {
//            Toast.makeText(this, "Вы приобрели пакет", Toast.LENGTH_LONG).show()
            showSuccessAlert()
        }
    }

    private fun createAndShowAlertDialog(offeringId: String?) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.warning_purchace_pack))
        builder.setPositiveButton(R.string.yes, { dialog, id ->
            //TODO
            addService("Add", offeringId)
//            subscribe(offeringId, position)
            dialog.dismiss()
        })
        builder.setNegativeButton(R.string.no, { dialog, id ->
            //TODO
            dialog.dismiss()
        })
        val dialog = builder.create()
        dialog.show()
    }

    private fun showSuccessAlert() {
        successIcon?.visibility = View.VISIBLE
        error?.text = getString(R.string.pack_purchace_success)
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, R.color.sucess))


        handler.postDelayed(this::hideAlert, 3000)
    }

    private fun showFailureAlert(text: String) {
        successIcon?.visibility = View.GONE
        error?.text = text
        val bottomUp = AnimationUtils.loadAnimation(this,
        R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_dark))



        handler.postDelayed(this::hideAlert, 3000)

    }

    private fun hideAlert() {
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_in_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.GONE

    }


}