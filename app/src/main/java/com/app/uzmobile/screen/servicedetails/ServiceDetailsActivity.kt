package com.app.uzmobile.screen.servicedetails

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.screen.packdetails.SectionsAdapter
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class ServiceDetailsActivity: BaseActivity() {
    companion object {
        const val PACK = "pack"
        const val TURNED = "turned"
    }

    var backBtn: ImageView? = null

    var title: TextView? = null

    var pack: TextView? = null

    var packValue: TextView? = null

    var internet: TextView? = null

    var expPeriod: TextView? = null

    var ussdUz: TextView? = null

    var ussdRu: TextView? = null

    var list: RecyclerView? = null

    var addService: Button? = null

    var alert: RelativeLayout? = null

    var successIcon: ImageView? = null

    var error: TextView? = null

    var handler: Handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packdetail)

        val service = intent.getSerializableExtra(PACK) as Service?
        val turned = service?.isChecked ?: false



        alert = findViewById(R.id.alert)

        error = findViewById(R.id.alertTitle)
        error?.text = getString(R.string.err_buy_service)

        successIcon  = findViewById(R.id.success_ic)

        backBtn = findViewById(R.id.menu)
        backBtn?.setOnClickListener { _ -> finish() }

        title = findViewById(R.id.title)
        title?.text = getString(R.string.services)

        addService = findViewById(R.id.add)
        addService?.setOnClickListener { _ -> createAndShowAlertDialog(service?.offeringId) }
        if (turned)
            addService?.text = getString(R.string.turnoff)

        pack = findViewById(R.id.pack)
        pack?.text = service?.name

        packValue = findViewById(R.id.balance)
        val balanceVal = service?.monthlyCost?.toFloat()?.times(4210.35f)
        packValue?.text = getString(R.string.balance_val, service?.monthlyCost)

        internet = findViewById(R.id.internet_payed)


        list = findViewById(R.id.list)
        list?.layoutManager = LinearLayoutManager(this)


        request(service?.offeringId)
    }

    private fun request(offeringId: String?) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account = null
//        header.Account.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.Service.name, null, "Info", offeringId, null))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.getService(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleTarifs, { t: Throwable? ->   t?.printStackTrace()})
    }

    private fun handleTarifs(res: JReceive<Tarifs>) {
        var adapter = SectionsAdapter(this,res.response?.query?.items?.sections)
        list?.adapter = adapter

    }

    private fun createAndShowAlertDialog(offeringId: String?) {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(getString(R.string.warning_confirm_action))
        dialogBuilder.setPositiveButton(R.string.yes, { dialog, id ->
            //TODO
            if(addService?.text?.equals(getString(R.string.add_service))!!) {
                addService("Add", offeringId)
            } else if (addService?.text?.equals(getString(R.string.turnoff))!!) {
                unsubscribe(offeringId)
            }
//            subscribe(offeringId, position)
            dialog.dismiss()
        })
        dialogBuilder.setNegativeButton(R.string.no, { dialog, id ->
            //TODO
            dialog.dismiss()
        })
        val dialog = dialogBuilder.create()
        dialog.show()
    }

    private fun addService(action: String = "Start", offeringId: String? = null) {
        error?.text = getString(R.string.err_buy_service)
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.SO.name, null, action, offeringId))
        val jsend = JSend(request)
        ApiFactory.inPayService!!.soRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
//                .map { res -> res.response?.query?.items!! }
                .subscribe(this::handleAddService, { _ -> Log.d("TAG", "error")})
    }


    private fun handleAddService(response : JReceive<List<Service>>) {
        if (!response.response?.error?.equals("0")!! || response.response?.query?.result?.equals("false")!!)
//            Toast.makeText(this, "Не удалось, попробуйте позже", Toast.LENGTH_LONG).show()
                showFailureAlert(response.response?.error)
        else {
            addService?.text = getString(R.string.turnoff)
            showSuccessAlert(getString(R.string.service_purchace_success))
//            Toast.makeText(this, "Вы подключили услугу", Toast.LENGTH_LONG).show()
        }
    }

    private fun unsubscribe(offeringId: String?) {
        error?.text = getString(R.string.err_turnoff_service)
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account = null
//        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val serviceSwitch = ServiceSwitch()
        serviceSwitch.action = "Stop"
        serviceSwitch.offeringId = offeringId
        val request = BaseRequest (header, Query(null, RequestType.ServiceOffering.name, null, "Stop", offeringId))
        val jsend = JSend(request)

        ApiFactory.inPayService!!.serviceSwitch(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleUnsubscribe, {t ->  })
    }

    private fun handleUnsubscribe(response: JReceive<Any>) {
        addService?.text = getString(R.string.add_service)
        showSuccessAlert(getString(R.string.service_turnoff_success))
    }

    private fun showSuccessAlert(text: String?) {
        successIcon?.visibility = View.VISIBLE
        error?.text = text
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, R.color.sucess))


        handler.postDelayed(this::hideAlert, 3000)
    }

    private fun showFailureAlert(text: String?) {
        successIcon?.visibility = View.GONE
        error?.text = text
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_dark))



        handler.postDelayed(this::hideAlert, 3000)

    }


    private fun showAlert() {
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE

        handler.postDelayed(this::hideAlert, 3000)

    }

    private fun hideAlert() {
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_in_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.GONE
    }

}
