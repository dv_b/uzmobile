package com.app.uzmobile.persistant;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Account extends RealmObject {
  @PrimaryKey
  public String number;

  public String token;

  public boolean isPrimary;
}
