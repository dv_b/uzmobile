package com.app.uzmobile.screen.landing

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.widget.*
import com.app.uzmobile.*
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.persistant.Account
import com.app.uzmobile.screen.addnumber.AddNumberActivity
import com.app.uzmobile.screen.chart.ChartActivity
import com.app.uzmobile.screen.chart.plus
import com.app.uzmobile.screen.general.ChooseLangDialog
import com.app.uzmobile.screen.language.LanguageActivity
import com.app.uzmobile.screen.packs.PacksActivity
import com.app.uzmobile.screen.rates.RatesActivity
import com.app.uzmobile.screen.registration.confirm.ConfirmActivity
import com.app.uzmobile.screen.services.ServicesActivity
import com.app.uzmobile.screen.sessions.SessionsActivity
import com.app.uzmobile.screen.web.WebActivity
import com.app.uzmobile.utils.CustomContextWrapper
import com.app.uzmobile.widgets.CircleProgressbar
import com.daimajia.slider.library.Indicators.PagerIndicator
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.DefaultSliderView
import com.github.mikephil.charting.charts.Chart.LOG_TAG
import io.realm.Realm
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class LandingActivity : BaseActivity(), ChooseLangDialog.Delegate, NumbersAdapter.Callback {

    override fun onClick(item: Account) {
        Realm.getDefaultInstance().executeTransaction { realm ->
            val items = realm.where(Account::class.java).findAll()
            for (i in items.indices) {
                items[i].isPrimary = false
                if (items[i].number.equals(item.number)) {
                    items[i].isPrimary = true
                }
            }
        }

        getPrefs().edit().putString(C.TOKEN, item.token).putString(C.LOGIN, item.number).apply()
        myNumber?.text = getString(R.string.number, item.number)
        myNumberDrawer?.text = getString(R.string.number, item.number)
        val result = Realm.getDefaultInstance().where(Account::class.java).findAll()
        for (ac in result) {
            Log.i("MUN", ac.number)
        }
        numbersAdapter?.refreshItems(result.filter { it -> !it.isPrimary })
        initRequest()



    }

    override fun chooseLang(lang: String?, country: String?) {
        changeLang(lang, country)
    }


    var drawer: DrawerLayout? = null

    var navigation: NavigationView? = null

    var title: TextView? = null

    var menu: ImageView? = null

    var balance: TextView? = null

    var tarif: TextView? = null

    var minutes: TextView? = null

    var mbytes: TextView? = null

    var sms: TextView? = null

    var minutesPercent: CircleProgressbar? = null

    var mbytesPercent: CircleProgressbar? = null

    var smsPercent: CircleProgressbar? = null

    var myNumber: TextView? = null

    var myNumberDrawer: TextView? = null

    var ratesView: LinearLayout? = null

    var servicesView: LinearLayout? = null

    var packsView: LinearLayout? = null

    var exit: Button? = null

    var adder: LinearLayout? = null

    var sessions: LinearLayout? = null

    var faq: LinearLayout? = null

    var market: LinearLayout? = null

    var langs: LinearLayout? = null

    var banner: ImageView? = null

    var tarifLabel: TextView? = null

    var servicesLabel: TextView? = null

    var packsLabel: TextView? = null

    var voiceContainer: RelativeLayout? = null

    var mbContainer: RelativeLayout? = null

    var smsContainer: RelativeLayout? = null

    var smsVal: PackUsageData? = null

    var gprsVal: Int? = null

    var voiceVal: Int? = null

    var slider: SliderLayout? = null

    var swipeRefresh: SwipeRefreshLayout? = null

    var version: TextView? = null

    var numbers: RecyclerView? = null

    var numbersAdapter: NumbersAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        if (getPrefs().getString(C.LANG, "ru").equals("en")) {
            chooseLang("ru", "RU")
        }

//        Crashlytics.getInstance().crash()

        initView()
        updateWidget()
//        initRequest()
//        request()
    }

    override fun onResume() {
        super.onResume()
        initRequest()
    }

    fun initView() {
        drawer = findViewById(R.id.drawer_layout)
        navigation = findViewById(R.id.navigation_view)

        title = findViewById(R.id.title)
//        setHelvetica(title)

        balance = findViewById(R.id.balance)
        balance?.text = getString(R.string.balance_val, getPrefs().getFloat(C.BALANCE, 0.0f).toString())

        tarif = findViewById(R.id.tarif)
        tarif?.text = getString(R.string.tarif, getPrefs().getString(C.TARIF, ""))

        minutes = findViewById(R.id.min)
        minutes?.text = getPrefs().getInt(C.MINUTES, 0).toString()
        mbytes = findViewById(R.id.mb)
        mbytes?.text = getPrefs().getInt(C.MB, 0).toString()
        sms = findViewById(R.id.sms_text)
        sms?.text = getPrefs().getInt(C.SMS, 0).toString()
        setHelvetica(minutes)
        setHelvetica(mbytes)
        setHelvetica(sms)

        voiceContainer = findViewById(R.id.voice_container)
        mbContainer = findViewById(R.id.mb_container)
        smsContainer = findViewById(R.id.sms_container)

        smsContainer?.setOnClickListener { _ ->
            if (!sms?.text?.equals("0")!!)
                startChartActivity(1)
        }
        mbContainer?.setOnClickListener { _ ->
            if (!mbytes?.text?.equals("0")!!)
                startChartActivity(2)
        }
        voiceContainer?.setOnClickListener { _ ->
            if (!minutes?.text?.equals("0")!!)
                startChartActivity(3)
        }

        sms?.setOnClickListener { _ ->
            if (!sms?.text?.equals("0")!!)
                startChartActivity(1)
        }
        mbytes?.setOnClickListener { _ ->
            if (!mbytes?.text?.equals("0")!!)
                startChartActivity(2)
        }
        minutes?.setOnClickListener { _ ->
            if (!minutes?.text?.equals("0")!!)
                startChartActivity(3)
        }

        minutesPercent = findViewById(R.id.minutes)
        minutesPercent?.color = ContextCompat.getColor(this, R.color.minutes_stroke)
        mbytesPercent = findViewById(R.id.mbytes)
        mbytesPercent?.color = ContextCompat.getColor(this, R.color.mb_stroke)
        mbytesPercent?.setSecondColor(ContextCompat.getColor(this, R.color.mb_second_pack))
        mbytesPercent?.setThirdColor(ContextCompat.getColor(this, R.color.mb_third_pack))
        smsPercent = findViewById(R.id.sms)
        smsPercent?.color = ContextCompat.getColor(this, R.color.sms_stroke)
        smsPercent?.setSecondColor(ContextCompat.getColor(this, R.color.sms_second_pack))

        myNumber = findViewById(R.id.number)
        myNumberDrawer = findViewById(R.id.num)

        myNumber?.text = getString(R.string.number, getPrefs().getString(C.LOGIN, ""))
        myNumberDrawer?.text = getString(R.string.number, getPrefs().getString(C.LOGIN, ""))

        ratesView = findViewById(R.id.rates_container)
        ratesView?.setOnClickListener { _ -> startRatesActivity() }

        servicesView = findViewById(R.id.services_container)
        servicesView?.setOnClickListener { _ -> startServicesActivity() }

        packsView = findViewById(R.id.packs_container)
        packsView?.setOnClickListener { _ -> startPacksActivity() }

        menu = findViewById(R.id.menu)
        menu?.setOnClickListener { _ ->  triggerMenu()}

        exit = findViewById(R.id.exit)
        exit?.setOnClickListener { _ ->
            removeData()
            startLoginActivity()
        }

        adder = findViewById(R.id.adder)
        adder?.setOnClickListener { _ -> addNumber() }

        sessions = findViewById(R.id.sessions)
        sessions?.setOnClickListener { _ -> startSessionsActivity()}

        faq = findViewById(R.id.faq)
        faq?.setOnClickListener { _ -> openFAQ() }

        market = findViewById(R.id.market)
        market?.setOnClickListener { _ -> openMarketDep() }

        langs = findViewById(R.id.langs)
        langs?.setOnClickListener { _ -> startLanguageActivity() }

//        banner = findViewById(R.id.banner)
//        banner?.setOnClickListener { _ -> openURL() }

        tarifLabel = findViewById(R.id.tarif_label)
        setHelvetica(tarifLabel)

        servicesLabel = findViewById(R.id.services_label)
        setHelvetica(servicesLabel)

        packsLabel = findViewById(R.id.packs_label)
        setHelvetica(packsLabel)

//        var list = Arrays.asList("50.0", "25.0", "12.0")
//        mbytesPercent?.setAllProgressWithAnimation(list)
//
//        smsPercent?.setAllProgressWithAnimation(list)
//        minutesPercent?.setAllProgressWithAnimation(list)

        slider = findViewById(R.id.slider)
        val textSliderView1 = DefaultSliderView(this)
        // initialize a SliderLayout
        textSliderView1
                .image(R.drawable.uzmobile_banner_1)
                .setScaleType(BaseSliderView.ScaleType.CenterInside)
//                .setOnSliderClickListener(this)

        //add your extra information
//        textSliderView1.bundle(Bundle())
//        textSliderView1.bundle
//                .putString("extra", "")

            slider?.addSlider(textSliderView1)

        slider = findViewById(R.id.slider)
        val textSliderView3 = DefaultSliderView(this)
        // initialize a SliderLayout
        textSliderView3
                .image(R.drawable.uzmobile_banner_3)
                .setScaleType(BaseSliderView.ScaleType.CenterInside)
//                .setOnSliderClickListener(this)

        //add your extra information
//        textSliderView3.bundle(Bundle())
//        textSliderView3.bundle
//                .putString("extra", "")

        slider?.addSlider(textSliderView3)

        slider = findViewById(R.id.slider)

        val textSliderView2 = DefaultSliderView(this)
        // initialize a SliderLayout
        textSliderView2
                .image(R.drawable.uzmobile_banner_2)
                .setScaleType(BaseSliderView.ScaleType.CenterInside)
//                .setOnSliderClickListener(this)

        //add your extra information
//        textSliderView2.bundle(Bundle())
//        textSliderView2.bundle
//                .putString("extra", "")

        slider?.addSlider(textSliderView2)

        slider?.setPresetTransformer(SliderLayout.Transformer.Default)
        slider?.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        slider?.indicatorVisibility = PagerIndicator.IndicatorVisibility.Invisible

//        slider?.setCustomAnimation(DescriptionAnimation())
        slider?.setDuration(4000)

        swipeRefresh = findViewById(R.id.swiperefresh)
        swipeRefresh?.setOnRefreshListener {   }

        swipeRefresh?.setOnRefreshListener(
                {
                    Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout")
                    initRequest()

                }
        )

        version = findViewById(R.id.version)
        version?.text = getString(R.string.version, BuildConfig.VERSION_NAME)

        numbers = findViewById(R.id.numbers)
        numbers?.layoutManager = GridLayoutManager(this, 2)
        val result = Realm.getDefaultInstance().where(Account::class.java).findAll()
        Log.i("TAG", result.size.toString())
        numbersAdapter = NumbersAdapter(this, result.filter { it -> !it.isPrimary })
        numbersAdapter?.callback = this
        numbers?.adapter = numbersAdapter

    }

    private fun startLanguageActivity() {
        val intent = Intent(this, LanguageActivity::class.java)
        intent.putExtra(LanguageActivity.CHANGE, true)
        startActivity(intent)
    }

    private fun openLangChooserDialog() {
        val dialog = ChooseLangDialog.getInstance()
        dialog.setDelegate(this)
        dialog.show(supportFragmentManager, "lang")
    }

    private fun startSessionsActivity() {
        startActivity(Intent(this, SessionsActivity::class.java))
    }


    private fun startChartActivity(flag: Int) {
        val intent = Intent(this, ChartActivity::class.java)
        if (flag == 1) {
            intent.putExtra(ChartActivity.TITLE, getString(R.string.sms_title))
            intent.putExtra(ChartActivity.TOTAL, smsVal?.totalAmount)
            intent.putExtra(ChartActivity.FLAG, flag)
        } else if (flag == 2) {
            intent.putExtra(ChartActivity.TITLE, getString(R.string.internet_title))
            intent.putExtra(ChartActivity.TOTAL, gprsVal)
            intent.putExtra(ChartActivity.FLAG, flag)
        } else if (flag == 3) {
            intent.putExtra(ChartActivity.TITLE, getString(R.string.minutes_title))
            intent.putExtra(ChartActivity.TOTAL, voiceVal)
            intent.putExtra(ChartActivity.FLAG, flag)
        }
        intent.putStringArrayListExtra(ChartActivity.PERCENTS, smsVal?.percents as ArrayList<String>?)
        startActivity(intent)
    }

    override fun onStop() {
        slider?.stopAutoCycle()
        super.onStop()
    }

    private fun addNumber() {
        startActivity(Intent(this, AddNumberActivity::class.java))
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up)
    }

    private fun startPacksActivity() {
        startActivity(Intent(this, PacksActivity::class.java))
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
        //todo -> remove data??

    }

    private fun startServicesActivity() {
        startActivity(Intent(this, ServicesActivity::class.java))
    }

    private fun startRatesActivity() {
        startActivity(Intent(this, RatesActivity::class.java))
    }

    private fun removeData() {
        val token = getPrefs().getString(C.TOKEN, "")
        deleteSession(token)
        getPrefs().edit().clear().apply()
        Realm.getDefaultInstance().executeTransaction { realm -> realm.deleteAll() }
        updateWidget()
    }

    private fun updateWidget() {
        val intent = Intent(this, MyUzmobileWidgetProvider::class.java)
        intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
// Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
// since it seems the onUpdate() is only fired on that:
        val ids = AppWidgetManager.getInstance(application).getAppWidgetIds(ComponentName(getApplication(), MyUzmobileWidgetProvider::class.java!!))
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        sendBroadcast(intent)
    }

    private fun changeLang(lang: String?, country: String?) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val query = Query<Any>(null, RequestType.Lang.name, null, null, null, null, null)
        query.value = lang
        val request = BaseRequest (header, query)
        val jsend = JSend(request)
        ApiFactory.inPayService!!.changeLang(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    setLocale(lang, country)
                    getPrefs().edit().putString(C.LANG, lang).apply()
                    updateWidget()
                    recreate()
                }, {

                })
    }

    private fun deleteSession(token: String?) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val query = Query<Any>(null, RequestType.Token.name, null, "Del")
        query.value = token
        val request = BaseRequest (header, query)
        val jsend = JSend(request)

        ApiFactory.inPayService!!.deleteSession(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {})
    }

    private fun initRequest() {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
//        header.Account.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.Balance.name, null))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.balanceRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleBalance, { _ -> Log.d("TAG", "error balance")})

    }

    private fun handleBalance(res : JReceive<String>) {
        swipeRefresh?.isRefreshing = false
        val ok = res.response?.query?.type.equals("sendNewPass")
        if (ok) {
            startActivity(Intent(this, ConfirmActivity::class.java))
            finish()
            return
        }

        val err = res.response?.error
        if (err?.equals("6.1")!!) {
            getPrefs().edit().clear().apply()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
            return
        }

//        val balanceVal = res.response?.query?.amount?.toFloat()?.times(4210.35f)
        val balanceVal = res.response?.query?.balance?.uzs//?.toFloat()//?.times(4210.35f)

        getPrefs().edit().putFloat(C.BALANCE, balanceVal!!).apply()


        balance?.text = getString(R.string.balance_val, balanceVal?.toString())
        tarifRequest()
        percentFreeUnitRequest()

    }


    private fun tarifRequest() {
        val tarifheader = Header()
        tarifheader.token = getPrefs().getString(C.TOKEN, "")
        tarifheader.Account = null
//        tarifheader.Account.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        tarifheader.Account.Pass = getPrefs().getString(C.PASSWORD, "")
        val tarifrequest = BaseRequest (tarifheader, Query(null, RequestType.CurrentTarif.name, null))
        val tarifjsend = JSend(tarifrequest)
        ApiFactory.sDevTest!!.currentTarif(tarifjsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleTarif, { _ -> Log.d("TAG", "error")})

    }



    private fun percentFreeUnitRequest() {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account = null
//        header.Account.ServiceNumber = getPrefs().getString(C.LOGIN, "")
//        header.Account.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.PercentFreeUnit.name, null))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.percentFreeUnits(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handlePercentFreeUnit, { _ -> Log.d("TAG", "error")})
    }

    private fun handlePercentFreeUnit(res: JReceive<PercentFreeUnit>) {

        val voiceAmount = res.response?.query?.items?.voice?.totalAmount?.toInt()
        val vMinutes = voiceAmount?.div(60)?.toInt()

        voiceVal = vMinutes

//        val percents = java.util.ArrayList<String>()
//        percents.add("70")
//        percents.add("30")
//        minutesPercent?.setAllProgressWithAnimation(percents)

        val voice = res.response?.query?.items?.voice?.details
        if (voice != null && vMinutes != null) {
            minutesPercent?.setAllProgressWithAnimation(getPercents(voice))
//            minutesPercent?.setAllProgressWithAnimation(res.response?.query?.items?.voice?.percents)
//            minutesPercent?.setAllProgressWithAnimation(res.response?.query?.items?.voice?.percents)


            minutes?.text = vMinutes.toString()
            getPrefs().edit().putInt(C.MINUTES, vMinutes).apply()
        }

        val gprsAmount = res.response?.query?.items?.gprs?.totalAmount?.toInt()
        val mBytes = gprsAmount?.div(1024)

        gprsVal = mBytes


        val gprs = res.response?.query?.items?.gprs?.details

        if (gprs != null && mBytes != null) {
            mbytesPercent?.setAllProgressWithAnimation(getPercents(gprs))
//            mbytesPercent?.setAllProgressWithAnimation(res.response?.query?.items?.gprs?.percents)
            mbytes?.text = mBytes.toString()
            getPrefs().edit().putInt(C.MB, mBytes).apply()
        }

        val smsAmount = res.response?.query?.items?.sms?.totalAmount

        this.smsVal = res.response?.query?.items?.sms
        val sms = res.response?.query?.items?.sms?.details
        if (sms != null) {
            smsPercent?.setAllProgressWithAnimation(getPercents(sms))
//            smsPercent?.setAllProgressWithAnimation(res.response?.query?.items?.sms?.percents)
            this.sms?.text = smsAmount?.toString()
            getPrefs().edit().putInt(C.SMS, smsAmount!!).apply()
        }

    }

    private fun getSum(vals: List<Detail>?): Float {
        var sum = 0.0f
        for (unused in vals!!) {
            sum = sum.plus(unused.initial?.toFloat())
        }
        return sum
    }


    private fun getPercents(details: List<Detail>): MutableList<String>? {
        val list = java.util.ArrayList<String>()
        for (detail in details) {
//            list.add(getPercent(total, detail.unused!!).toString())
            list.add(detail.persent!!.toString())
        }
        return list
    }

    private fun getPercent(total: Int, value: Int): Float {
        return value / total * 1.0f
    }


    private fun handleTarif(res: JReceive<Tarif>) {
        tarif?.text = getString(R.string.tarif, res.response?.query?.items?.name ?: getString(R.string.not_defined))
        getPrefs().edit().putString(C.TARIF, res.response?.query?.items?.name).apply()
    }

    private fun triggerMenu() {
        if (drawer!!.isDrawerOpen(GravityCompat.START))
            drawer?.closeDrawer(GravityCompat.START)
        else
            drawer?.openDrawer(GravityCompat.START)

    }

    private fun openFAQ() {
        val url = "http://m.uzmobile.uz/ru/feedback/"

        val i = Intent(this, WebActivity::class.java)
        i.putExtra(WebActivity.LINK, url)
        i.putExtra(WebActivity.TITLE, getString(R.string.feedback))
//        i.data = Uri.parse(url)
        startActivity(i)
    }

    private fun openMarketDep() {
        val url = "http://m.uzmobile.uz/ru/contacts/offices/tashkent/"

        val i = Intent(this, WebActivity::class.java)
        i.putExtra(WebActivity.LINK, url)
        i.putExtra(WebActivity.TITLE, getString(R.string.market_dep))
//        i.data = Uri.parse(url)
        startActivity(i)
    }

    private fun openURL() {
        val url = "m.uzmobile.uz"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    fun setLocale(localeValue: String?, country: String?) {
        var localeValue = localeValue
        if (localeValue == null || localeValue.isEmpty())
            localeValue = "ru"

        var country = country
        if (TextUtils.isEmpty(country)) {
            country = "Ru"
        }

        CustomContextWrapper.wrap(this, localeValue, country)
    }

}