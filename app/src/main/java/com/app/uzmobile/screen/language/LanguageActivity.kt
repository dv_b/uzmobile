package com.app.uzmobile.screen.language

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.MainActivity
import com.app.uzmobile.R
import com.app.uzmobile.screen.landing.LandingActivity
import com.app.uzmobile.utils.CustomContextWrapper


class LanguageActivity: BaseActivity() {

    companion object {
        const val CHANGE = "change"
    }

    enum class LANG {
        en, ru, uz, def
    }

    var lang = LANG.def

    var uz: ImageView? = null

    var uzo: ImageView? = null

    var ru: ImageView? = null

    var ruo: ImageView? = null

    var en: ImageView? = null

    var eno: ImageView? = null

    var submit: Button? = null

    var alert: LinearLayout? = null

    var handler: Handler = Handler(Looper.getMainLooper())

    var change = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)

        change = intent.getBooleanExtra(CHANGE, false)

        alert = findViewById(R.id.alert)

        uz = findViewById(R.id.uz)
        uzo = findViewById(R.id.uzo)

        uz?.setOnClickListener({v ->
            uzo?.visibility = View.VISIBLE
            ruo?.visibility = View.GONE
            eno?.visibility = View.GONE

            lang = LANG.uz

            getPrefs().edit().putString(C.LANG, lang.name).apply()
            setLocale("uz", "Uz")
            if (change)
                startActivity(Intent(this, LandingActivity::class.java))
            else
                startLoginActivity()
        })
        ru = findViewById(R.id.ru)
        ruo = findViewById(R.id.ruo)
        ru?.setOnClickListener({ v ->
            uzo?.visibility = View.GONE
            ruo?.visibility = View.VISIBLE
            eno?.visibility = View.GONE

            lang = LANG.ru

            getPrefs().edit().putString(C.LANG, lang.name).apply()
            setLocale("ru", "Ru")
            if (change)
                startActivity(Intent(this, LandingActivity::class.java))
            else
                startLoginActivity()
        })
//        en = findViewById(R.id.en)
//        eno = findViewById(R.id.eno)
//        en?.setOnClickListener({ v ->
//            uzo?.visibility = View.GONE
//            ruo?.visibility = View.GONE
//            eno?.visibility = View.VISIBLE
//
//            lang = LANG.en
//
//            getPrefs().edit().putString(C.LANG, lang.name).apply()
//            setLocale("en", "US")
//            startLoginActivity()
//        })

//        submit = findViewById(R.id.submit)
//        submit?.setOnClickListener({view ->
//            if (lang != LANG.def) {
//                getPrefs().edit().putString(C.LANG, lang.name).apply()
//                startLoginActivity()
//            } else {
//                showAlert()
//            }
//        })

    }

    fun setLocale(localeValue: String?, country: String?) {
        var localeValue = localeValue
        if (localeValue == null || localeValue.isEmpty())
            localeValue = "ru"

        var country = country
        if (TextUtils.isEmpty(country)) {
            country = "Ru"
        }

        CustomContextWrapper.wrap(this, localeValue, country)
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun showAlert() {
        val animate = TranslateAnimation(0f, 0f, 0f, (alert?.getHeight()!!.times(1.0)).toFloat())
        animate.duration = 400
        animate.fillAfter = true
        (alert as LinearLayout).startAnimation(animate)
        (alert as LinearLayout).setVisibility(View.VISIBLE)

        handler.postDelayed(this::hideAlert, 3000)

    }

    private fun hideAlert() {
        val animate = TranslateAnimation(0f, 0f, (alert?.getHeight()!!.times(1.0)).toFloat(), 0f)
        animate.duration = 400
        animate.fillAfter = true
        (alert as LinearLayout).startAnimation(animate)
        (alert as LinearLayout).setVisibility(View.GONE)
    }
}