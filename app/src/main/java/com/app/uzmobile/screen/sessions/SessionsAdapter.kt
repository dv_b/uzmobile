package com.app.uzmobile.screen.sessions

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.app.uzmobile.R
import com.app.uzmobile.content.Session


class SessionsAdapter(context: Context, items: MutableList<Session>): RecyclerView.Adapter<SessionsAdapter.SessionViewHolder>() {

    var context = context

    var list = items

    var callback: SessionsAdapterCallback? = null

    init {
        if (list.size > 1) {
            val deleteAll = Session()
            deleteAll.description = context.getString(R.string.delete_all)
            deleteAll.type = 2
            list.add(1, deleteAll)
        }

    }

    fun deleteItem(position: Int) {
        list.removeAt(position)
        if (list.size == 2)
            list.removeAt(1)
        notifyDataSetChanged()
    }

    fun deleteItem(item: Session?) {
        list.remove(item)
        if (list.size == 2)
            list.removeAt(1)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: SessionViewHolder?, position: Int) {
        holder?.name?.text = list.get(position).description
        holder?.delete?.text = list.get(position).description

        holder?.del?.setOnClickListener({v ->  callback?.call(list.get(position))})

        if (holder?.delete != null) {
             holder?.itemView?.setOnClickListener({view ->
                 callback?.deleteAll(list.subList(1, list.size))
             })
         }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SessionViewHolder {
        if (viewType == 0)
            return SessionViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_session, parent, false))
        if (viewType == 1)
            return SessionViewHolder(LayoutInflater.from(context).inflate(R.layout.item_session, parent, false))
        if (viewType == 2)
            return SessionViewHolder(LayoutInflater.from(context).inflate(R.layout.item_delete_sessions, parent, false))

        return SessionViewHolder(LayoutInflater.from(context).inflate(R.layout.item_session, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return list[position].type!!
    }

    inner class SessionViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {
        var name = itemView?.findViewById<TextView>(R.id.name)
        var del = itemView?.findViewById<ImageView>(R.id.del)
        var current = itemView?.findViewById<TextView>(R.id.current)

        var delete = itemView?.findViewById<TextView>(R.id.title)
    }

}