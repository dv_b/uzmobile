package com.app.uzmobile.screen.rates.adapters

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.app.uzmobile.R
import com.app.uzmobile.content.Service


class RatesAdapter(context: Context, items: MutableList<Service>, isUsd: Boolean = true) : RecyclerView.Adapter<RatesAdapter.ViewHolder>() {

    var list: MutableList<Service> = items
    var mContext = context
    var mIsUsd = isUsd

    val helveticaLight = Typeface.createFromAsset(context.assets, "HelveticaNeue.ttf")

    var callback: AdapterCalback? = null
    set(value) {
        field = value
    }

    override fun getItemCount(): Int {
       return list.size
    }

    fun setItems(items: List<Service>) {
        list.addAll(items)
        notifyDataSetChanged()
    }

    fun refreshItems(items: List<Service>) {
        list.clear()
        list.addAll(items)
        notifyDataSetChanged()
    }

    fun setItem(item: Service) {
        list.add(item)
        notifyDataSetChanged()
    }



    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.name?.text = list.get(position).name

//        if (mIsUsd) {
//            val balanceVal = list.get(position).monthlyCost?.replace(" ", "")?.replace(",", ".")?.toFloat()?.times(4210.35f)
//            holder?.cost?.text = mContext.getString(R.string.balance_val, balanceVal)
//        } else {
            val balanceVal = list.get(position).monthlyCost//?.replace(" ", "")?.replace(",", ".")?.toFloat()?.times(4210.35f)
        if (isNumeric(balanceVal!!))
            holder?.cost?.text = mContext.getString(R.string.balance_val, balanceVal?.toFloat())
//        }
        holder?.itemView?.setOnClickListener { _ -> showDetails(position) }
        holder?.sumscribe?.setOnClickListener { _ -> showDetails(position) }
    }

    fun isNumeric(input: String): Boolean =
            try {
                input.toDouble()
                true
            } catch(e: NumberFormatException) {
                false
            }


    private fun  showDetails(position: Int) {
        callback?.call(list[position], position)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_rate, parent, false))
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var name = itemView?.findViewById<TextView>(R.id.name)
        var cost = itemView?.findViewById<TextView>(R.id.cost)
        var sumscribe = itemView?.findViewById<ImageView>(R.id.subscribe)

//        init {
//            name?.typeface = helveticaLight
//        }

    }


}