package com.app.uzmobile.widgets;


import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

public class CustomValueFormatter implements IValueFormatter, IAxisValueFormatter {

  protected DecimalFormat mFormat;

  private int maxValue;

  private String type;

  public CustomValueFormatter(int maxValue, String type) {
    mFormat = new DecimalFormat("###.0");
    this.maxValue = maxValue;
    this.type = type;
  }

  @Override
  public String getFormattedValue(float value, AxisBase axis) {
    if (value  < 10) return "";
    return mFormat.format(value * maxValue / 100) + " " + type;
  }

  @Override
  public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
    if (value  < 10) return "";
    double v = value;
    return mFormat.format(v * maxValue / 100) + " " + type;
  }
}
