package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName

class Packet {
    @SerializedName("Type")
    var type: String? = null

    @SerializedName("Name")
    var name: String? = null

    @SerializedName("Unused")
    var unused: List<Unused>? = null
}
