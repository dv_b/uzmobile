package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName

class Query<T>(t: T?, type: String? = null, amount: String? = null, action: String? = null, offeringId: String? = null, lang: String? = "ru", code: String? = null) {
    @SerializedName("Result")
    var result: String? = null

    @SerializedName("Items")
    var items = t

    @SerializedName("Balance")
    var balance: Balance? = null

    @SerializedName("Type")
    var type: String? = type

    @SerializedName("Action")
    var action: String? = action

    @SerializedName("OfferingId")
    var offeringId: String? = offeringId

    @SerializedName("Amount")
    var amount: String? = amount

    @SerializedName("Lang")
    var lang:String? = lang

    @SerializedName("Value")
    var value:String? = null

    @SerializedName("Code")
    var code: String? = code

    @SerializedName("Description")
    var description: String? = null
}

