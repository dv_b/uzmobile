package com.app.uzmobile.content


enum class RequestType {
    Token,
    Balance,
    FreeUnit,
    TotalFreeUnit,
    PercentFreeUnit,
    CurrentTarif,
    SO,
    PO,
    PurchasedSO,
    ServiceOffering,
    Lang,
    Service;



}