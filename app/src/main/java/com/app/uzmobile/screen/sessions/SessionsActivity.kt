package com.app.uzmobile.screen.sessions

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.ImageView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.MainActivity
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.widgets.SimpleDividerItemDecoration
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class SessionsActivity: BaseActivity(), SessionsAdapterCallback {
    override fun deleteAll(subList: MutableList<Session>) {
        var i = 2
        for (session in subList) {
            call(session)
        }
    }

    override fun call(session: Session?) {
        deleteSession(session)
    }

    var list: RecyclerView? = null

    var adapter: SessionsAdapter? = null

    var back: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sessions)

        list = findViewById(R.id.list)
        list?.layoutManager = LinearLayoutManager(this)
        list?.addItemDecoration(SimpleDividerItemDecoration(this))

        back = findViewById(R.id.menu)
        back?.setOnClickListener { _ -> finish() }

        sessionsRequest()

    }

    private fun sessionsRequest() {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.Token.name, null, "List"))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.sessionsRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleSessions, {_ -> Log.d("TAG", "error sessions")})
    }

    private fun handleSessions(res: JReceive<MutableList<Session>>) {
        val items = reorderList(res.response?.query?.items!!)

        adapter = SessionsAdapter(this, items)
        adapter?.callback = this
        list?.adapter = adapter
    }

    private fun reorderList(list: MutableList<Session>): MutableList<Session> {
        val current = getPrefs().getString(C.TOKEN, "")
        for (i in list.indices) {
            if (list.get(i).token?.equals(current)!!) {
                val foundSess = list.get(i)
                foundSess.type = 0
                list.removeAt(i)
                list.add(0, foundSess)
            } else {
                list[i].type = 1
            }
        }
        return list
    }

    private fun deleteSession(session: Session?) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val query = Query<Any>(null, RequestType.Token.name, null, "Del")
        query.value = session?.token
        val request = BaseRequest (header, query)
        val jsend = JSend(request)

        ApiFactory.sDevTest!!.deleteSession(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({_ -> adapter?.deleteItem(session) }, {})
    }

    private fun removeData() {
        getPrefs().edit().clear().apply()
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}