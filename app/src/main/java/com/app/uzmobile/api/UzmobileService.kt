package com.app.uzmobile.api

import com.app.uzmobile.content.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable


interface UzmobileService {

    @POST("SharePoint/UzMobileApp.php/")
    fun balanceRequest(@Body request: JSend): Observable<JReceive<String>>

    @POST("SharePoint/UzMobileApp.php/")
    fun freeUnitRequest(@Body request: JSend): Observable<JReceive<List<Packet>>>

    @POST("SharePoint/UzMobileApp.php/")
    fun totalFreeUnitsRequest(@Body request: BaseRequest): Observable<BaseResponse<String>>

    @POST("SharePoint/UzMobileApp.php/")
    fun percentFreeUnits(@Body request: JSend): Observable<JReceive<PercentFreeUnit>>

    @POST("SharePoint/UzMobileApp.php/")
    fun currentTarif(@Body request: JSend): Observable<JReceive<Tarif>>

    @POST("SharePoint/UzMobileApp.php/")
    fun soRequest(@Body request: JSend): Observable<JReceive<List<Service>>>

    @POST("SharePoint/UzMobileApp.php/")
    fun serviceSwitch(@Body request: JSend): Observable<JReceive<Any>>

    @POST("SharePoint/UzMobileApp.php/")
    fun sessionsRequest(@Body request: JSend): Observable<JReceive<MutableList<Session>>>

    @POST("SharePoint/UzMobileApp.php/")
    fun deleteSession(@Body request: JSend): Observable<JReceive<String>>

    @POST("SharePoint/UzMobileApp.php/")
    fun changeLang(@Body request: JSend): Observable<JReceive<String>>

    @GET("CRM/ManageServices/pakets/AppPacketInfo.php")
    fun request(@Query("OfferingId") offeringId: String, @Query("Lang") lang: String = "ru"): Observable<Tarifs>

    @GET("CRM/ManageServices/uslugi/AppUslugiInfo.php")
    fun services(@Query("OfferingId") offeringId: String, @Query("Lang") lang: String = "ru"): Observable<Tarifs>

    @GET("CRM/ManageServices/tarifs/AppTarifInfo.php")
    fun tarifs(@Query("OfferingId") offeringId: String, @Query("Lang") lang: String = "ru"): Observable<Tarifs>





}