package com.app.uzmobile.screen.registration.confirm

import android.Manifest
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.*
import android.support.v13.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.persistant.Account
import com.app.uzmobile.receivers.SmsReceiver
import com.app.uzmobile.screen.landing.LandingActivity
import io.realm.Realm
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class ConfirmActivity : BaseActivity() {

    val OTP_REGEX = "[0-9]{1,6}"

    var login: EditText? = null
    var submit: Button? = null
    var alert: LinearLayout? = null

    var number: String? = null

    var time: TextView? = null

    var timeContainer: LinearLayout? = null

    var handler: Handler = Handler(Looper.getMainLooper())

    var min = 2
    var sec = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sms)

        number = intent.getStringExtra(C.LOGIN)
        if (TextUtils.isEmpty(number)) {
            number = getPrefs().getString(C.LOGIN, "")
        }

        login = findViewById(R.id.login)

//        login?.addTextChangedListener(TextWatcher() {
//
//        })
//        setArrowActive()
        alert = findViewById(R.id.alert)

        submit = findViewById(R.id.submit)
        submit?.setOnClickListener { _ ->
            if (submit?.text?.equals(getString(R.string.repeat))!!) {
                login()
                waitingMode()
            } else if (submit?.text?.equals(getString(R.string.confirm))!!) {
                confirm()
            }
        }

        time = findViewById(R.id.time)

        timeContainer = findViewById(R.id.time_container)

        requestSmsPermission()

        runTimer()


        login()


    }

    private fun requestSmsPermission() {
        val permission = Manifest.permission.READ_SMS
        val grant = ContextCompat.checkSelfPermission(this, permission)
        if (grant != PackageManager.PERMISSION_GRANTED) {
            val permission_list = arrayOfNulls<String>(1)
            permission_list[0] = permission
            ActivityCompat.requestPermissions(this, permission_list, 1)
        } else {
            setSmsListener()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(this@AccountClass, "permission granted", Toast.LENGTH_SHORT).show()
                    setSmsListener()

            } else {
//                Toast.makeText(this@AccountClass, "permission not granted", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun setSmsListener() {
        val i = IntentFilter("android.provider.Telephony.SMS_RECEIVED")
        registerReceiver(SmsReceiver(), i)

        SmsReceiver.bindListener { messageText ->
            //From the received text string you may do string operations to get the required OTP
            //It depends on your SMS format
            Log.e("Message", messageText)

            login?.setText(messageText)
            confirm()
//            Toast.makeText(this, "Message: " + messageText, Toast.LENGTH_LONG).show()
//
//            // If your OTP is six digits number, you may use the below code
//
//            val pattern = Pattern.compile(OTP_REGEX)
//            val matcher = pattern.matcher(messageText)
//            var otp = ""
//            while (matcher.find()) {
//                otp = matcher.group()
//            }
//
//            Toast.makeText(this, "OTP: " + otp, Toast.LENGTH_LONG).show()
        }
    }

    private fun setArrowActive() {
        var drawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_forward_black_24dp)
        drawable = DrawableCompat.wrap(drawable)
        DrawableCompat.setTint(drawable, Color.WHITE)
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN)

        login?.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
    }

    private fun setArrowPassive() {
        var drawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_forward_black_24dp)
        drawable = DrawableCompat.wrap(drawable)
        DrawableCompat.setTint(drawable, Color.GRAY)
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN)

        login?.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
    }

    private fun waitingMode() {
//        submit?.visibility = View.GONE
        submit?.text = getString(R.string.confirm)
        timeContainer?.visibility = View.VISIBLE

        runTimer()
    }

    private fun resubmitMode() {
        submit?.text = getString(R.string.repeat)
        timeContainer?.visibility = View.GONE

        handler.removeCallbacksAndMessages(null)

        time?.text = "2:00"
        min = 2
        sec = 0

    }

    private fun runTimer() {

         object: CountDownTimer(120000, 1000) {

             override fun onTick(millisUntilFinished: Long) {
                 var seconds = ""


                 if (sec == 0) {
                     min -= 1
                     sec = 59
                 } else {
                     sec -= 1
                 }


                 if (sec < 10)
                     seconds = "0" + sec.toString()
                 else
                     seconds = sec.toString()

                 time?.text = getString(R.string.time, min, seconds)


             }

             override fun onFinish() {
                 resubmitMode()
             }
          }.start()
//        handler.postDelayed({
//            var loop = true
//            var seconds = ""
//            var ctime = System.currentTimeMillis()
//            while (loop) {
//                if (System.currentTimeMillis() - ctime >= 1000) {
//                    if (min == 0 && sec == 0) {
//                        loop = false
//                        resubmitMode()
//                    }
//
//                    if (sec < 10)
//                        seconds = "0" + sec.toString()
//                    else
//                        seconds = sec.toString()
//
//                    time?.text = getString(R.string.time, min, seconds)
//
//                    if (sec == 0)
//                        sec = 59
//                    else
//                        sec -= 1
//
//                    if (sec == 0)
//                        min -= 1
//
//
//                    ctime = System.currentTimeMillis()
//                }
//
//
//
//            }
//
//        }, 1000)

    }

    private fun login() {
        if (number != null) {
            val header = Header()
            header.Account?.ServiceNumber = number
            val lang = getPrefs().getString(C.LANG, "ru")
            val query = Query<Any>(null, RequestType.Token.name, null, "Registration", null, lang)
            query.description = getDeviceName() + "\nAndroid " + android.os.Build.VERSION.RELEASE
            val request = BaseRequest(header, query)
            val jsend = JSend(request)
            ApiFactory.sDevTest!!.balanceRequest(jsend)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleLogin, { _ ->
                        Log.d("TAG", "error")
//                        showAlert()
                    })
        }
    }

    private fun handleLogin(res: JReceive<String>) {
        val ok = res.response?.error?.equals("1.23")
        if (ok != null && ok) {
            login()
            return
        }


        val status = res.response?.error?.equals("0")
        if (status != null && !status) {
//            showAlert()
            return
        }
//        if (res.response?.query?.result.equals("true", false)) {
        getPrefs().edit().putString(C.LOGIN, number).apply()
//        }
    }


    fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else {
            capitalize(manufacturer) + " " + model
        }
    }


    private fun capitalize(s: String?): String {
        if (s == null || s.length == 0) {
            return ""
        }
        val first = s[0]
        return if (Character.isUpperCase(first)) {
            s
        } else {
            Character.toUpperCase(first) + s.substring(1)
        }
    }



    fun confirm() {
        if (isValid()) {
            val header = Header()
            header.Account?.ServiceNumber = number
//            header.Account.Pass = login!!.text.toString()
            val request = BaseRequest(header, Query(null, RequestType.Token.name, null,
                    "Confirm", null, null, login!!.text.toString()))
            val jsend = JSend(request)
            ApiFactory.sDevTest!!.balanceRequest(jsend)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleConfirm, { _ -> Log.d("TAG", "error") })
        }
    }


    private fun handleConfirm(res: JReceive<String>) {
        val ok = res.response?.error?.equals("0")
        if (ok!!) {
            val account = Account()
            account.number = number
            account.token = res.response?.query?.value
            saveNumber(account)

            startActivity(Intent(this, LandingActivity::class.java))
            finish()
            return
        }

        //todo -> show alert
        showFailureAlert()

    }

    private fun  saveNumber(account: Account) {

        Realm.getDefaultInstance().executeTransaction {
            realm ->
            val result = realm.where(Account::class.java).findAll()
            if (!result.isEmpty()) {
                for (ac in result) {
                    ac.isPrimary = false
                }
            }

            account.isPrimary = true
            getPrefs().edit().putString(C.PASSWORD, login!!.text.toString())
                    .putString(C.TOKEN, account.token).apply()
            realm.copyToRealmOrUpdate(account)
        }
    }

    fun isValid(): Boolean {
        if (TextUtils.isEmpty(login!!.text.toString())) {
            return false
        }
        return true
    }


    private fun showFailureAlert() {
//        successIcon?.visibility = View.GONE
//        error?.text = text
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.VISIBLE
        alert?.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_dark))



        handler.postDelayed(this::hideAlert, 3000)

    }

    private fun hideAlert() {
        val bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.slide_in_up)
        alert?.startAnimation(bottomUp)
        alert?.visibility = View.GONE

    }

}