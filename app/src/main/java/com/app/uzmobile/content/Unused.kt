package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName

class Unused {

    @SerializedName("Value")
    var value: String? = null

    @SerializedName("ExpireDateTimeshtamp")
    var expireDateTimestamp: String? = null

    @SerializedName("ExpireDate")
    var expire: String? = null
}
