package com.app.uzmobile.screen.rates

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.app.uzmobile.BaseActivity
import com.app.uzmobile.C
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.persistant.RealmTarif
import com.app.uzmobile.screen.ratedetails.RateDetailsActivity
import com.app.uzmobile.screen.rates.adapters.AdapterCalback
import com.app.uzmobile.screen.rates.adapters.RatesAdapter
import com.app.uzmobile.utils.RealmConvertor
import com.app.uzmobile.widgets.SimpleDividerItemDecoration
import com.github.lzyzsd.circleprogress.Utils
import io.realm.Realm
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*


class RatesActivity : BaseActivity(), AdapterCalback {
    override fun call(item: Service, position: Int) {
        val intent = Intent(this, RateDetailsActivity::class.java)
        intent.putExtra(RateDetailsActivity.RATE, item)
        startActivity(intent)
    }

//    var tabs: TabLayout? = null
//    var pager: ViewPager? = null

//    var pagerAdapter: PagerAdapter? = null

    var list: RecyclerView? = null

    var items: List<String>? = null

    var adapter: RatesAdapter? = null

    var backButton: ImageView? = null

    var emptyList: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rates)

//        tabs = findViewById(R.id.tab) as TabLayout?
//        tabs?.newTab()?.setText(R.string.my_rate)?.let { tabs?.addTab(it) }
//        tabs?.newTab()?.setText(R.string.all_rates)?.let { tabs?.addTab(it) }
//        tabs?.tabGravity = TabLayout.GRAVITY_FILL
//
//        pager = findViewById(R.id.pager) as ViewPager?
//
//        val list = arrayListOf<Fragment>(MyRateFragment("MY"), MyRateFragment("MY"))
//
//        pagerAdapter = PagerAdapter(supportFragmentManager, list)
//
//        pager?.adapter = pagerAdapter
//        pager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
////        tabs?.setupWithViewPager(pager)

        adapter = RatesAdapter(this, ArrayList<Service>(), false)
        adapter?.callback = this


        val manager = LinearLayoutManager(this)

        list = findViewById(R.id.list)
        list?.addItemDecoration(SpaceItemDecorator(Utils.dp2px(resources, 2.0F).toInt()))
        list?.layoutManager = manager
        list?.addItemDecoration(SimpleDividerItemDecoration(this))
        list?.adapter = adapter
//        adapter?.setItems(getMockRates())

        backButton = findViewById(R.id.menu)
        backButton?.setOnClickListener { _ -> finish() }

        emptyList = findViewById(R.id.empty_list)

        soRequest()

    }

    private fun soRequest(action: String = "List", offeringId: String? = null) {
        val header = Header()
        header.token = getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.PO.name, null, action, offeringId, getPrefs().getString(C.LANG, "ru")))
        val jsend = JSend(request)
        ApiFactory.sDevTest!!.soRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map { res -> res.response?.query?.items!! }
                .subscribe(this::handleServices, { _ ->
                    val result = Realm.getDefaultInstance().where(RealmTarif::class.java).findAll()
                    if (result.isEmpty()) {
                        setEmptyListVisibility()
                    } else {
                        handleServices(RealmConvertor.getTarifs(result))
                    }
                })
    }

    private fun handleServices(items : List<Service>) {
        if (items.isEmpty()) {
            setEmptyListVisibility()
        } else {
            setListVisibility()
            saveCache(items)
            adapter?.setItems(items.sortedWith(Service))
        }
//        for (i in items) {
////            if (i.name?.contains("MB", ignoreCase = false) as Boolean)
//            adapter?.setItem(i)
//        }
    }

    private fun  saveCache(items: List<Service>?) {
        Realm.getDefaultInstance().executeTransaction {
            realm -> realm.copyToRealmOrUpdate(RealmConvertor.getRealmTarifs(items)) }
    }

    private fun setEmptyListVisibility() {
        emptyList?.visibility = View.VISIBLE
        list?.visibility = View.GONE
    }

    private fun setListVisibility() {
        emptyList?.visibility = View.GONE
        list?.visibility = View.VISIBLE
    }

    private fun getMockRates() : List<String> {
        return listOf("STEP", "SALOM", "SALOM PLUS", "UZMOBILE 1200", "UZMOBILE 4000")
    }
}