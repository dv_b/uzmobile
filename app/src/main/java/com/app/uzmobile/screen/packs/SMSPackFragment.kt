package com.app.uzmobile.screen.packs

import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import com.app.uzmobile.C
import com.app.uzmobile.MainActivity
import com.app.uzmobile.R
import com.app.uzmobile.api.ApiFactory
import com.app.uzmobile.content.*
import com.app.uzmobile.persistant.RealmService
import com.app.uzmobile.screen.general.BaseFragment
import com.app.uzmobile.screen.rates.SpaceItemDecorator
import com.app.uzmobile.screen.rates.adapters.AdapterCalback
import com.app.uzmobile.screen.rates.adapters.RatesAdapter
import com.app.uzmobile.utils.RealmConvertor
import com.app.uzmobile.widgets.SimpleDividerItemDecoration
import com.github.lzyzsd.circleprogress.Utils
import io.realm.Realm
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class SMSPackFragment: BaseFragment() {
    var adapter: RatesAdapter? = null

    var list: RecyclerView? = null

    var swipeRefresh: SwipeRefreshLayout? = null

    var empty: LinearLayout? = null

    var items: List<Service> = ArrayList()

    var callback: AdapterCalback? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        adapter = RatesAdapter(context, ArrayList())
    }

    override fun bind(view: View?) {
        val manager = LinearLayoutManager(context)

        list = view?.findViewById(R.id.list)
        list?.addItemDecoration(SpaceItemDecorator(Utils.dp2px(resources, 2.0F).toInt()))
        list?.layoutManager = manager
        list?.addItemDecoration(SimpleDividerItemDecoration(context))
        list?.adapter = adapter

        empty = view?.findViewById(R.id.empty_list)
        empty?.setOnClickListener({v ->
            adapter?.list?.clear()
            adapter?.notifyDataSetChanged()
            empty?.visibility = View.GONE
            soRequest()})

        swipeRefresh = view?.findViewById(R.id.swiperefresh)
        swipeRefresh?.setOnRefreshListener {
            adapter?.list?.clear()
            adapter?.notifyDataSetChanged()
            empty?.visibility = View.GONE
            soRequest()
        }

        if (!items.isEmpty())
            adapter?.setItems(items)

        if (callback != null)
            adapter?.callback = callback


    }

    private fun soRequest(action: String = "P_List", offeringId: String? = null) {
        val header = Header()
        header.token = (activity as PacksActivity).getPrefs().getString(C.TOKEN, "")
        header.Account?.ServiceNumber = (activity as PacksActivity).getPrefs().getString(C.LOGIN, "")
        header.Account?.Pass = (activity as PacksActivity).getPrefs().getString(C.PASSWORD, "")
        val request = BaseRequest (header, Query(null, RequestType.SO.name, null, action, offeringId))
        val jsend = JSend(request)
        ApiFactory.inPayService!!.soRequest(jsend)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map { res ->
                    val err = res.response?.error
                    if (err?.equals("6.1")!!) {
                        (activity as PacksActivity).getPrefs().edit().clear().apply()
                        startActivity(Intent(context, MainActivity::class.java))
                        activity.finish()
                    }
                    res.response?.query?.items!!
                }
                .subscribe(this::handleServices, { _ ->
                    swipeRefresh?.isRefreshing = false
                    val result = Realm.getDefaultInstance().where(RealmService::class.java).findAll()
                    if (result.isEmpty()) {
                        list?.visibility = View.GONE
                        empty?.visibility = View.VISIBLE
                    } else {
                        handleServices(RealmConvertor.getServices(result))
                    }
                })
    }

    private fun handleServices(items : List<Service>) {
        if (items.isEmpty()) {
            list?.visibility = View.GONE
            empty?.visibility = View.VISIBLE
        } else {
            list?.visibility = View.VISIBLE
            empty?.visibility = View.GONE
            adapter?.refreshItems(items.filter { predicate -> predicate.unitType.equals("sms", true) })
        }
        swipeRefresh?.isRefreshing = false
    }

    fun setData(services: List<Service>) {
        if (services.isEmpty()) {
            list?.visibility = View.GONE
            empty?.visibility = View.VISIBLE
        } else {
            list?.visibility = View.VISIBLE
            empty?.visibility = View.GONE
            adapter?.setItems(services)
            items = services
        }

    }

    override val layoutId: Int
        get() = R.layout.pack_list

}
