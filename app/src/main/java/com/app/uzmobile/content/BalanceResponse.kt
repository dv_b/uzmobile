package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName


class BalanceResponse {

    @SerializedName("Type")
    var type: String? = null

    @SerializedName("Amount")
    var amount: String? = null

    @SerializedName("Result")
    var result: String? = null
}