package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName


class PercentFreeUnit {

    @SerializedName("Voice")
    var voice: PackUsageData? = null

    @SerializedName("GPRS")
    var gprs: PackUsageData? = null

    @SerializedName("SMS")
    var sms: PackUsageData? = null
}