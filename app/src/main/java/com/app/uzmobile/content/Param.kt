package com.app.uzmobile.content

import com.google.gson.annotations.SerializedName

class Param {
    @SerializedName("item_name")
    var itemName: String? = null

    var value: String? = null

    @SerializedName("icon_name")
    var iconName: String? = null

    var texts: String? = null
}

